#!/bin/bash
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER threedots PASSWORD 'supertajnehaslo12';
    CREATE DATABASE threedotsdb;
    GRANT ALL PRIVILEGES ON DATABASE threedotsdb TO threedots;
EOSQL