#Odpalenie projektu:
aby odpalic projekt, nalezy wlaczyc skrypt:
- devops/env/localhost/stop-services.sh

#Zatrzymanie dockerow:
aby zatrzymac dockery, nalezy odpalic skrypt:
- devops/env/localhost/stop-services.sh

#Przebudowanie samej aplikacji, bez bazy danych
nalezy odpalic skrypt:
- devops/env/localhost/rebuild-and-run.sh


#Swagger odstepny na
http://localhost:8080/swagger-ui/index.html#/

#Dane admina oraz ominiecie security
username: admin

haslo: haslo

id: 100

aby ominac security dla wszystkich endpointow, w przypadku admina, nalezy jako parametr url podac 
"?userId=100", badz zalogowac sie jako admin

badz w przypadku zwyklych użytkowników podajac ich ID, np "?userId=21"
