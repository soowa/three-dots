package pl.mastalerz.threedots.domain;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import pl.mastalerz.threedots.domain.product.Discount;

import java.math.BigDecimal;
import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class DiscountTest {

    @ParameterizedTest(name = "{index} => for=\"{0}\", expectedResult={1}")
    @ArgumentsSource(DiscountTestDataSet.class)
    void shouldCreateObject(BigDecimal argument, boolean expectedResult) {
        if (expectedResult) {
            assertThatCode(() -> {
                Discount.of(argument);
            }).doesNotThrowAnyException();
        } else {
            assertThatThrownBy(() -> {
                Discount.of(argument);
            }).isInstanceOf(IllegalArgumentException.class);
        }
    }


    static class DiscountTestDataSet implements ArgumentsProvider {
        private final static boolean VALID = true;
        private final static boolean INVALID = false;

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
            return Stream.of(
                    argumentWith(BigDecimal.valueOf(10), VALID),
                    argumentWith(BigDecimal.valueOf(120), VALID),
                    argumentWith(BigDecimal.valueOf(0), VALID),
                    argumentWith(BigDecimal.valueOf(-0.5), INVALID),
                    argumentWith(BigDecimal.valueOf(-1), INVALID),
                    argumentWith(BigDecimal.valueOf(-32), INVALID)
            );
        }

        private Arguments argumentWith(BigDecimal argument, boolean expectedResult) {
            return Arguments.of(argument, expectedResult);
        }

    }
}