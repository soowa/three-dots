package pl.mastalerz.threedots.domain;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import pl.mastalerz.threedots.domain.product.Name;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class NameTest {

    @ParameterizedTest(name = "{index} => for=\"{0}\", expectedResult={1}")
    @ArgumentsSource(NameTestDataSet.class)
    void shouldCreateObject(String argument, boolean expectedResult) {
        if (expectedResult) {
            assertThatCode(() -> {
                Name.of(argument);
            }).doesNotThrowAnyException();
        } else {
            assertThatThrownBy(() -> {
                Name.of(argument);
            }).isInstanceOf(IllegalArgumentException.class);
        }
    }


    static class NameTestDataSet implements ArgumentsProvider {
        private final static boolean VALID = true;
        private final static boolean INVALID = false;

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
            return Stream.of(
                    argumentWith("", INVALID),
                    argumentWith(" ", INVALID),
                    argumentWith("example", VALID),
                    argumentWith(null, INVALID),
                    argumentWith("example example", VALID)

            );
        }

        private Arguments argumentWith(String argument, boolean expectedResult) {
            return Arguments.of(argument, expectedResult);
        }

    }

}