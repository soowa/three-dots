package pl.mastalerz.threedots.domain;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import pl.mastalerz.threedots.domain.stock.StockAmount;

import java.util.stream.Stream;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

class StockAmountTest {

    @ParameterizedTest(name = "{index} => for=\"{0}\", expectedResult={1}")
    @ArgumentsSource(StockAmountTestDataSet.class)
    void shouldCreateObject(int argument, boolean expectedResult) {
        if (expectedResult) {
            assertThatCode(() -> {
                StockAmount.of(argument);
            }).doesNotThrowAnyException();
        } else {
            assertThatThrownBy(() -> {
                StockAmount.of(argument);
            }).isInstanceOf(IllegalArgumentException.class);
        }
    }


    static class StockAmountTestDataSet implements ArgumentsProvider {
        private final static boolean VALID = true;
        private final static boolean INVALID = false;

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
            return Stream.of(
                    argumentWith(10, VALID),
                    argumentWith(11, VALID),
                    argumentWith(0, VALID),
                    argumentWith(-1, INVALID),
                    argumentWith(-10, INVALID),
                    argumentWith(-110, INVALID)
            );
        }

        private Arguments argumentWith(int argument, boolean expectedResult) {
            return Arguments.of(argument, expectedResult);
        }

    }

}