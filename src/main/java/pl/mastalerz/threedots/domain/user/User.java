package pl.mastalerz.threedots.domain.user;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.address.Address;

import javax.persistence.*;

@Getter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "users")
public class User extends BaseEntity {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "nick"))
    private Nick nick;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "email"))
    private Email email;

    @Column(name = "password")
    private String password;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "first_name"))
    private FirstName firstName;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "last_name"))
    private LastName lastName;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "phone_number"))
    private PhoneNumber phoneNumber;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    private UserRole role;

    public static User create(Nick nick,
                              Email email,
                              String password,
                              FirstName firstName,
                              LastName lastName,
                              PhoneNumber phoneNumber,
                              Address address) {
        return new User(
                nick, email, password, firstName, lastName, phoneNumber, address, UserRole.CLIENT
        );
    }
}
