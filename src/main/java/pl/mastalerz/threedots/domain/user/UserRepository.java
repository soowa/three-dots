package pl.mastalerz.threedots.domain.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAllByRole(UserRole role);

    Optional<User> findByNick(Nick nick);

    Optional<User> findByEmail(Email email);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN 'true' ELSE 'false' END FROM User p WHERE lower(p.nick.value) like lower(:nick)")
    boolean isNickAlreadyUsed(String nick);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN 'true' ELSE 'false' END FROM User p WHERE lower(p.email.value) like lower(:email)")
    boolean isEmailAlreadyUsed(String email);
}
