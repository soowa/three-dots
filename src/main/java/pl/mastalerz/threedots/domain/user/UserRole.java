package pl.mastalerz.threedots.domain.user;

public enum UserRole {
    CLIENT, ADMIN
}
