package pl.mastalerz.threedots.domain.user;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;
import java.util.regex.Pattern;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Email {
    static final String regexPattern = "^(.+)@(\\S+)$";
    private String value;

    private Email(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect email value");

        this.value = value;
    }

    public static Email of (String value){
        return new Email(value);
    }

    private boolean isValid(String value){
        final var isBlank = StringUtils.isBlank(value);

        final var isRegexCorrect = Pattern.compile(Email.regexPattern)
                .matcher(value)
                .matches();

        return !isBlank && isRegexCorrect;
    }
}
