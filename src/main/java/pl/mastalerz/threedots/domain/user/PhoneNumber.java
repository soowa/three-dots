package pl.mastalerz.threedots.domain.user;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;
import java.util.regex.Pattern;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PhoneNumber {
    private static final String regexPattern = "^\\d{10}$";
    private String value;

    private PhoneNumber(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect nick value");

        this.value = value;
    }

    public static PhoneNumber of (String value){
        return new PhoneNumber(value);
    }

    private boolean isValid(String value){
        final var isBlank = StringUtils.isBlank(value);

        final var isRegexCorrect = Pattern.compile(PhoneNumber.regexPattern)
                .matcher(value)
                .matches();

        return !isBlank && isRegexCorrect;
    }
}
