package pl.mastalerz.threedots.domain.user;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Nick {
    private String value;

    private Nick(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect nick value");

        this.value = value;
    }

    public static Nick of (String value){
        return new Nick(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }


}
