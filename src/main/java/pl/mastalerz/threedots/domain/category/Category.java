package pl.mastalerz.threedots.domain.category;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;

import javax.persistence.*;

@Getter
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "category")
public class Category extends BaseEntity {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "name"))
    private CategoryName name;

    public void updateName(CategoryName name){
        this.name = name;
    }

}
