package pl.mastalerz.threedots.domain.category;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("select s.name from Category s")
    List<String> findAllNames();

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN 'true' ELSE 'false' END FROM Category p WHERE lower(p.name.value) like lower(:name)")
    boolean existsByName_Value(String name);
}
