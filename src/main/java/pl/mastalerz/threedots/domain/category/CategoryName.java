package pl.mastalerz.threedots.domain.category;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;


@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class CategoryName {
    private String value;

    private CategoryName(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect category name value");

        this.value = value;
    }

    public static CategoryName of (String value){
        return new CategoryName(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value) && value.length() <= 20;
    }

}
