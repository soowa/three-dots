package pl.mastalerz.threedots.domain.product;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Description {
    private String value;

    private Description(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect desciption value");

        this.value = value;
    }

    public static Description of (String value){
        return new Description(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }
}
