package pl.mastalerz.threedots.domain.product;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Name {
    private String value;

    private Name(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect name value");

        this.value = value;
    }

    public static Name of (String value){
        return new Name(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }

}
