package pl.mastalerz.threedots.domain.product;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Price {

    public static final Price ZERO = Price.of(BigDecimal.ZERO);

    private BigDecimal value;

    private Price(BigDecimal value) {
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect price value");

        this.value = value;
    }

    public static Price of(BigDecimal value) {
        return new Price(value);
    }

    public Price add(Price price) {
        return new Price(value.add(price.getValue()));
    }

    public Price substract(Price price) {
        return new Price(value.subtract(price.getValue()));
    }

    public Price multiply(int amount) {
        return Price.of(value.multiply(BigDecimal.valueOf(amount)));
    }

    @Override
    public String toString() {
        // TODO: Implement toString method

        // 5 -> 5,00
        // 10 -> 10,00
        // 1.5 -> 1,50
        // 1.05 -> 1,05
        return value.toString();
    }

    private boolean isValid(BigDecimal value) {
        return BigDecimal.ZERO.compareTo(value) <= 0;
    }
}
