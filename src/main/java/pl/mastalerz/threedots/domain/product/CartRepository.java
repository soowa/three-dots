package pl.mastalerz.threedots.domain.product;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.mastalerz.threedots.domain.user.Nick;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Long> {

    Optional<Cart> findCartByUser_Nick(Nick nick);


}
