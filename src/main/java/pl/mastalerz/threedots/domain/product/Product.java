package pl.mastalerz.threedots.domain.product;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.category.Category;
import pl.mastalerz.threedots.domain.productReview.ProductReview;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.size.SizeLabel;
import pl.mastalerz.threedots.domain.variant.Variant;
import pl.mastalerz.threedots.exception.DoesNotExistException;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Getter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "product")
public class Product extends BaseEntity {

    @Column(name = "model_number")
    private UUID modelNumber;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "name"))
    private Name name;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "price"))
    private Price price;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "description"))
    private Description description;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "discount"))
    private Discount discount;

    @Column(name = "discount_expiration_date")
    private LocalDateTime discountExpirationDate;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "product")
    private List<Variant> variants;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "product_id")
    private List<ProductReview> productReviews;

    public void updateDiscount(Discount discount, LocalDateTime dateTime) {
        this.discount = discount;
        this.discountExpirationDate = dateTime;
    }

    public CartItem cartItemOf(Long variantId, Size size) {
        final var variant = getVariant(variantId)
                .orElseThrow(() -> new DoesNotExistException("No variant with id " + variantId + " with size " + size));

        return CartItem.of(this, variant, size);
    }

    public Optional<Variant> getVariant(Long variantId) {
        return variants.stream()
                .filter(v -> v.getId().equals(variantId))
                .findAny();
    }

    public void setDiscountExpirationDate(LocalDateTime localDateTime) {
        this.discountExpirationDate = localDateTime;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Price getEffectivePrice() {
        if (discount == null) {
            return price;
        }
        final var priceValue = price.getValue();
        final var discountValue = discount.getValue();

        return Price.of(priceValue.subtract(discountValue).max(BigDecimal.ZERO));
    }

    public void setName(Name name) {
        this.name = name;
    }

    public void setDescription(Description description) {
        this.description = description;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void addVariant(Variant variant) {
        this.variants.add(variant);
    }

    public void deleteAllVariants() {
        this.variants.clear();
    }

    public void removeVariant(Variant variant) {
        this.variants.remove(variant);
    }

    public void setModelNumber(UUID variantNumber) {
        this.modelNumber = variantNumber;
    }

    public void addReview(ProductReview review) {
        this.productReviews.add(review);
    }

    public void deleteReview(ProductReview review) {
        this.productReviews.remove(review);
    }

    public void deleteAllReviews() {
        this.productReviews.clear();
    }

}
