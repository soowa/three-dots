package pl.mastalerz.threedots.domain.product;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.variant.Variant;

import javax.persistence.*;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "cart_item")
public class CartItem extends BaseEntity {

    @Column(name = "amount")
    private int amount;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name="variant_id")
    private Variant variant;

    @ManyToOne
    @JoinColumn(name = "size_id")
    private Size size;

    @ManyToOne
    private Cart cart;

    protected static CartItem of(Product product, Variant variant, Size size) {
        return new CartItem(1, product, variant, size, null);
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public void increase() {
        amount++;
    }

    public void decrease() {
        amount--;
    }

    public Price getPrice() {
        return product.getEffectivePrice().multiply(amount);
    }

    protected void setCart(Cart cart) {
        this.cart = cart;
    }
}

