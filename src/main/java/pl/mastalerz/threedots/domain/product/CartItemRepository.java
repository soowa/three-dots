package pl.mastalerz.threedots.domain.product;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {
    void deleteByVariantId(Long variantId);
}
