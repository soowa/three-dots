package pl.mastalerz.threedots.domain.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mastalerz.threedots.domain.category.Category;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
/*
* Model ->
* -name
* -modelNumber
* -List<Variant>
*   Variant
*   -color
*   -variantNumber
*   -price
*   List<Size>
    -size
    -stock
*
* */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    List<Product> findAllByCategoryId(Long categoryId);

    @Query("select s from Product s join fetch s.variants where s.id = :productId")
    Optional<Product> findByProductId(Long productId);

//    @Query("select s from Product s join fetch s.variants where s.modelNumber = :number")
//    Optional<Product> findByModelNumber(UUID number);

    //    @Query("select v from Product p join fetch p.variants v where v.id = :productId")
//    List<Variant> findVariantsByProductId(Long productId);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN 'true' ELSE 'false' END FROM Product p WHERE lower(p.name.value) like lower(:name)")
    boolean existsByName_Value(String name);

    @Modifying
    @Query("UPDATE Product p SET p.name.value = :name WHERE p.name.value = :toUpdate")
    void updateName(String toUpdate, String name);

    @Modifying
    @Query("UPDATE Product p SET p.category = null WHERE p.category = :category")
    void deleteCategoryFromProducts(Category category);

//    @Query("DELETE FROM Product p WHERE lower(p.name.value) like lower(:name)")
//    void testDelete(Long id);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN 'true' ELSE 'false' END FROM Product p WHERE p.modelNumber = :variantNumber AND p.name.value like :name")
    boolean isNameTheSame(UUID variantNumber, String name);
}