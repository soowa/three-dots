package pl.mastalerz.threedots.domain.product;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Discount {

    private BigDecimal value;

    private Discount(BigDecimal value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect price value");

        this.value = value;
    }

    public static Discount of (BigDecimal value){
        return new Discount(value);
    }

    private boolean isValid(BigDecimal value){
        return BigDecimal.ZERO.compareTo(value) <= 0;
    }

}
