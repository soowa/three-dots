package pl.mastalerz.threedots.domain.product;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.user.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "cart")
public class Cart extends BaseEntity {

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cart_id")
    private List<CartItem> items;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "total_price"))
    private Price totalPrice;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    public static Cart createEmptyFor(User user) {
        return new Cart(new ArrayList<>(), Price.ZERO, user);
    }

    public Price addItem(CartItem item) {
        getCartItem(item.getVariant().getId())
                .ifPresentOrElse(
                        CartItem::increase,
                        () -> {
                            item.setCart(this);
                            items.add(item);
                        }
                );
        totalPrice = recalculatePrice();
        return totalPrice;
    }

    public Price removeItem(CartItem item) {
        item.setCart(null);
        items.remove(item);
        totalPrice = recalculatePrice();
        return totalPrice;
    }

    public Optional<CartItem> getCartItem(Long variantId){
        return items.stream()
                .filter(x -> x.getVariant().getId().equals(variantId))
                .findFirst();
    }

    public void refresh() {
        totalPrice = recalculatePrice();
    }

    private Price recalculatePrice() {
        return Price.of(
                items.stream()
                        .map(CartItem::getPrice)
                        .map(Price::getValue)
                        .reduce(BigDecimal.ZERO, BigDecimal::add)
        );
    }
}
