package pl.mastalerz.threedots.domain.variant;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Color {
    private String value;

    private Color(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect color value");

        this.value = value;
    }

    public static Color of (String value){
        return new Color(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }
}
