package pl.mastalerz.threedots.domain.variant;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.product.Product;

import javax.persistence.*;
import java.util.UUID;

@Getter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "variant")
public class Variant extends BaseEntity {

    @Column(name = "variant_number")
    private UUID variantNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "image_url")
    private String imageUrl;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "color"))
    private Color color;

}
