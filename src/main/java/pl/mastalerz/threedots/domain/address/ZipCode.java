package pl.mastalerz.threedots.domain.address;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;
import java.util.regex.Pattern;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ZipCode {
    static final String regexPattern = "^([0-9]{2})(-[0-9]{3})?$";
    private String value;

    private ZipCode(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect zipCode value");

        this.value = value;
    }

    public static ZipCode of (String value){
        return new ZipCode(value);
    }

    private boolean isValid(String value){
        final var isBlank = StringUtils.isBlank(value);

        final var isRegexCorrect = Pattern.compile(ZipCode.regexPattern)
                .matcher(value)
                .matches();

        return !isBlank && isRegexCorrect;
    }
}
