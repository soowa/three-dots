package pl.mastalerz.threedots.domain.address;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import pl.mastalerz.threedots.domain.user.Nick;

import javax.persistence.Embeddable;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class City {
    private String value;

    private City(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect city value");

        this.value = value;
    }

    public static City of (String value){
        return new City(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }

}
