package pl.mastalerz.threedots.domain.address;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Street {
    private String value;

    private Street(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect city value");

        this.value = value;
    }

    public static Street of (String value){
        return new Street(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }
}
