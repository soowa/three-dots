package pl.mastalerz.threedots.domain.address;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Getter
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BuildingNumber {
    private String value;

    private BuildingNumber(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect city value");

        this.value = value;
    }

    public static BuildingNumber of (String value){
        return new BuildingNumber(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }
}
