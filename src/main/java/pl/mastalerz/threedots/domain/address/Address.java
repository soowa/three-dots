package pl.mastalerz.threedots.domain.address;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.user.Nick;

import javax.persistence.*;

@Getter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "address")
public class Address extends BaseEntity {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "city"))
    private City city;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "zip_code"))
    private ZipCode zipCode;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "street"))
    private Street street;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "building_number"))
    private BuildingNumber buildingNumber;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "apartment_number"))
    private ApartmentNumber apartmentNumber;
}
