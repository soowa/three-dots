package pl.mastalerz.threedots.domain.size;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;

import javax.persistence.*;

@Getter
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "size")
public class Size extends BaseEntity {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "size_label"))
    private SizeLabel sizeLabel;

}
