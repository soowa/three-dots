package pl.mastalerz.threedots.domain.size;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class SizeLabel {
    private String value;

    private SizeLabel(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect size value");

        this.value = value;
    }

    public static SizeLabel of (String value){
        return new SizeLabel(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value) && value.length() <= 2;
    }
}
