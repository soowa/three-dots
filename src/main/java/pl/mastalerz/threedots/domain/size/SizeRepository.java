package pl.mastalerz.threedots.domain.size;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SizeRepository extends JpaRepository<Size, Long> {

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN 'true' ELSE 'false' END FROM Size p WHERE lower(p.sizeLabel.value) like lower(:value)")
    boolean existsBySizeLabel_Value(String value);

    Optional<Size> findBySizeLabel(SizeLabel label);

//    @Query("SELECT * FROM Size p WHERE lower(p.sizeLabel.value) like lower(:value)")
//    List<Size> findAllByVariantNumber(UUID variantNumber);

}
