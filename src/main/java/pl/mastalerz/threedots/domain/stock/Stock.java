package pl.mastalerz.threedots.domain.stock;


import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.variant.Color;
import pl.mastalerz.threedots.domain.variant.Variant;

import javax.persistence.*;

@Getter
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "stock")
public class Stock extends BaseEntity {
    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "amount"))
    private StockAmount amount;

    @ManyToOne
    @JoinColumn(name = "variant_id")
    private Variant variant;

    @ManyToOne
    @JoinColumn(name = "size_id")
    private Size size;

    public boolean hasEnough(int n) {
        return amount.getValue() >= n;
    }

    public boolean isAvailable() {
        return amount.notZero();
    }

    public boolean isEmpty() {
        return !isAvailable();
    }

    public void updateStockAmount(StockAmount stockAmount){
        this.amount = stockAmount;
    }

    public Color getColor() {
        return getVariant().getColor();
    }

}
