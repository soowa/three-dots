package pl.mastalerz.threedots.domain.stock;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class StockAmount {
    private int value;

    private StockAmount(int value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect amount value");

        this.value = value;
    }

    public boolean notZero() {
        return value != 0;
    }

    public StockAmount substract(StockAmount stockAmount){
        return StockAmount.of(value - stockAmount.getValue());
    }

    public static StockAmount of (int value){
        return new StockAmount(value);
    }

    private boolean isValid(int value){
        return value >= 0;
    }

}
