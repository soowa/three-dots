package pl.mastalerz.threedots.domain.stock;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.variant.Variant;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface StockRepository extends JpaRepository<Stock, Long> {


//    @Query("SELECT p FROM Stock p WHERE p.variant.variantNumber = :variantNumber")
    List<Stock> findAllByVariant_Id(Long variantId);

    //    @Query("select s from Product s join fetch s.variants where s.id = :productId")
    @Query("SELECT s from Stock s WHERE s.variant.product.id = :productId")
    List<Stock> findAllByProductId(Long productId);
//    Optional<Stock> findStockByVariantUUID(UUID variantNumber);

    Optional<Stock> findByVariant_VariantNumberAndSize(UUID variantNumber, Size size);
    Optional<Stock> findByVariantIdAndSize(Long variantId, Size size);

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN 'true' ELSE 'false' END FROM Stock p WHERE p.size.id =:sizeId AND p.variant.id =:variantId")
    boolean sizeAlreadyExistsForVariant(Long sizeId, Long variantId);

    void deleteAllByVariantId(Long variantId);
    void deleteAllByVariant_VariantNumber(UUID variantId);

    @Modifying
    @Query("DELETE FROM Stock s WHERE s.variant = :variant")
    void deleteAllByVariant(Variant variant);
}
