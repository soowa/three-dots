package pl.mastalerz.threedots.domain.productReview;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class ReviewRating {
    private int value;

    private ReviewRating(int value){
        if (!isValid(value))
            throw new IllegalArgumentException("Rating must be between 1 and 4");

        this.value = value;
    }

    public static ReviewRating of (int value){
        return new ReviewRating(value);
    }

    private boolean isValid(int value){
        return value >= 1 && value <= 4;
    }

}
