package pl.mastalerz.threedots.domain.productReview;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class ReviewContent {
    private String value;

    private ReviewContent(String value){
        if (!isValid(value))
            throw new IllegalArgumentException("Incorrect content value");

        this.value = value;
    }

    public static ReviewContent of (String value){
        return new ReviewContent(value);
    }

    private boolean isValid(String value){
        return !StringUtils.isBlank(value);
    }

}
