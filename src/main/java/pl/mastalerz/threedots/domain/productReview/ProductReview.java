package pl.mastalerz.threedots.domain.productReview;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;

import javax.persistence.*;

@Getter
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "PRODUCT_REVIEW")
public class ProductReview extends BaseEntity {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "content"))
    private ReviewContent content;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "rating"))
    private ReviewRating rating;

}
