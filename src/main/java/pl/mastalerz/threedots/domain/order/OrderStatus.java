package pl.mastalerz.threedots.domain.order;

public enum OrderStatus {
    AWAITS_PAYMENT, CLOSED;
}
