package pl.mastalerz.threedots.domain.order;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.user.User;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Getter
@Table(name = "orders")
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Order extends BaseEntity {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "number"))
    private OrderNumber number;

    @Embedded
    private OrderAddress address;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "order_id")
    private List<OrderItem> orderItems;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Column(name = "first_name")
    private String ownerFirstName;

    @Column(name = "last_name")
    private String ownerLastName;

    @Column(name = "owner_id")
    private Long ownerId;

    public static Order createNew(
            OrderAddress address,
            List<OrderItem> items,
            User user
    ) {
        var order =  new Order(
                OrderNumber.of(UUID.randomUUID().toString()),
                address,
                items,
                OrderStatus.AWAITS_PAYMENT,
                user.getFirstName().getValue(),
                user.getLastName().getValue(),
                user.getId()
        );
        items.forEach(e -> e.setOrder(order));
        return order;
    }
}
