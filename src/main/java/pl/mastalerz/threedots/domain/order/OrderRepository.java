package pl.mastalerz.threedots.domain.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    // Not closed ORDERS
    @Query("FROM Order o WHERE o.status <> 'CLOSED' AND o.ownerId = :ownerId")
    List<Order> findActiveForOwner(Long ownerId);

    List<Order> findByOwnerId(Long ownerId);
}
