package pl.mastalerz.threedots.domain.order;

import lombok.*;
import pl.mastalerz.threedots.domain.BaseEntity;
import pl.mastalerz.threedots.domain.product.Price;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Getter
@Table(name = "order_item")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OrderItem extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "variant_number")
    private UUID variantNumber;

    @Column(name = "amount")
    private int amount;

    public OrderItem(String name, UUID variantNumber,
                     int amount, String size, String color,
                     Price unitPrice, Price totalPrice) {
        this.name = name;
        this.variantNumber = variantNumber;
        this.amount = amount;
        this.size = size;
        this.color = color;
        this.unitPrice = unitPrice;
        this.totalPrice = totalPrice;
    }

    @Column(name = "size")
    private String size;

    @Column(name = "color")
    private String color;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "unit_price"))
    private Price unitPrice;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "total_price"))
    private Price totalPrice;

    @ManyToOne
    @Setter(value = AccessLevel.PROTECTED)
    private Order order;

}
