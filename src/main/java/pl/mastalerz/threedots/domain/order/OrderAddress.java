package pl.mastalerz.threedots.domain.order;

import lombok.*;
import pl.mastalerz.threedots.domain.address.*;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;

@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class OrderAddress {

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "city"))
    private City city;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "zip_code"))
    private ZipCode zipCode;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "street"))
    private Street street;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "building_number"))
    private BuildingNumber buildingNumber;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "apartment_number"))
    private ApartmentNumber apartmentNumber;
}
