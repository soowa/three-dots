package pl.mastalerz.threedots.domain.order;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Embeddable;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class OrderNumber {

    private String value;

    private OrderNumber(String value) {
        this.value = value;
    }

    public static OrderNumber of(String value) {
        if (!isValid(value)) {
            throw new IllegalArgumentException("OrderNumber cannot be blank");
        }
        return new OrderNumber(value);
    }

    private static boolean isValid(String value) {
        return StringUtils.isNotBlank(value);
    }
}
