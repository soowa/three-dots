package pl.mastalerz.threedots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThreeDotsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThreeDotsApplication.class, args);
    }

}
