package pl.mastalerz.threedots.readmodel;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "product_view")
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductView {

    @Id
    @Column(name = "variant_id")
    private Long variantId;

    @Column(name = "product_id")
    private Long productId;
    private String name;
    private String color;
    private BigDecimal price;
    private String description;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "discount_expiration_date")
    private LocalDateTime discountExpirationDate;
    private BigDecimal discount;

    @Column(name = "category_id")
    private Long categoryId;

    @Column(name = "category_name")
    private String category;
}
