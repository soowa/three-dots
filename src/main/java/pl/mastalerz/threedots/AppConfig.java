package pl.mastalerz.threedots;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry myCorsRegistry){
        myCorsRegistry.addMapping("/**")
                .allowedOrigins("http://localhost:3000")  //frontend's link
                .allowedHeaders("*")
                .allowedMethods("*")
                .allowCredentials(true)
                .maxAge(4800);
    }
}
