package pl.mastalerz.threedots;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    public static final String SECURITY_SCHEMA_NAME = "bearerAuth";

    @Bean
    public OpenAPI reservationOpenApi() {
        return new OpenAPI()
//                .addSecurityItem(new SecurityRequirement().addList(SECURITY_SCHEMA_NAME))
                .components(
                        new Components()
                                .addSecuritySchemes(SECURITY_SCHEMA_NAME, new SecurityScheme()
                                        .name(SECURITY_SCHEMA_NAME)
                                        .type(SecurityScheme.Type.HTTP)
                                        .scheme("bearer")
                                        .bearerFormat("JWT")
                                )
                )
                .info(
                        new Info()
                                .title("ThreeDots API")
                                .description("ThreeDots - shop")
                                .version("v0.0.1")
                );
    }

    @Bean
    public GroupedOpenApi adminApi() {
        return GroupedOpenApi.builder()
                .group("Admin")
                .pathsToMatch("/api/admin/**")
                .build();
    }

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("Publiczne")
                .pathsToMatch("/api/public/**")
                .build();
    }

    @Bean
    public GroupedOpenApi allApis() {
        return GroupedOpenApi.builder()
                .group("Dla użytkownika")
                .pathsToExclude("/api/admin/**", "/api/auth/**")
                .build();
    }
}
