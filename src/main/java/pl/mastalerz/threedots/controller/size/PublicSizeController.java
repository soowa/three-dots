package pl.mastalerz.threedots.controller.size;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.size.SizeFacade;
import pl.mastalerz.threedots.dto.SizeResponseDto;

import java.util.List;

@RestController
@RequestMapping("api/public/sizes")
@RequiredArgsConstructor
@Tag(name = ApiTags.SIZE)
public class PublicSizeController {
    private final SizeFacade sizeFacade;

    @Operation(summary = "Endpoint zwracający pojedynczy rozmiar")
    @GetMapping("/{sizeId}")
    public ResponseEntity<SizeResponseDto> findById(@PathVariable Long sizeId) {
        var size = sizeFacade.findById(sizeId);
        return ResponseEntity.ok(size);
    }

    @Operation(summary = "Endpoint zwracający wszystkie rozmiary")
    @GetMapping("/all")
    public ResponseEntity<List<SizeResponseDto>> findAll() {
        var size = sizeFacade.findAll();
        return ResponseEntity.ok(size);
    }
}
