package pl.mastalerz.threedots.controller.size;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.size.SizeFacade;
import pl.mastalerz.threedots.dto.SizeCreateDto;
import pl.mastalerz.threedots.dto.SizeResponseDto;

import java.util.List;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequestMapping("api/admin/sizes")
@RequiredArgsConstructor
@Tags({@Tag(name = ApiTags.ADMIN), @Tag(name = ApiTags.SIZE)})
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class AdminSizeController {
    private final SizeFacade sizeFacade;

    @Operation(summary = "Utworzenie rozmiaru", description = "Wartość słownikowa")
    @PostMapping("/create")
    public ResponseEntity<Long> create(@RequestBody SizeCreateDto dto) {
        Long size = sizeFacade.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(size);
    }

    @Operation(summary = "Usunięcie rozmiaru")
    @DeleteMapping("/{sizeId}/remove")
    public ResponseEntity<Object> deleteSize(@PathVariable("sizeId") Long sizeId) {
        sizeFacade.delete(sizeId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Operation(summary = "Endpoint pozwalający usunąc wszystkie rozmiary ( ͡° ͜ʖ ͡°) (tak, wszystkich! - sic!)")
    @DeleteMapping("/remove-all")
    public ResponseEntity<Object> deleteSizes() {
        sizeFacade.deleteAll();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}