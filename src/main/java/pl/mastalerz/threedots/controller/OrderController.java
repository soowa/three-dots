package pl.mastalerz.threedots.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.OrderService;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
@Tag(name = ApiTags.ORDER)
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/{id}")
    @Operation(summary = "Dane zamówienia")
    public ResponseEntity getOrder(@PathVariable Long id) {
        return ResponseEntity.ok(orderService.getOrder(id));
    }

    @GetMapping("/{userId}/active")
    @Operation(summary = "Aktywne zamówienia dla użytkownika")
    public ResponseEntity getActiveOrdersForUser(@PathVariable Long userId) {
        return ResponseEntity.ok(orderService.getActiveOrdersForUser(userId));
    }

    @GetMapping("/{userId}/all")
    @Operation(summary = "Wszystkie zamówienia dla użytkownika")
    public ResponseEntity getAllOrdersForUser(@PathVariable Long userId) {
        return ResponseEntity.ok(orderService.getAllOrdersForUser(userId));
    }
}
