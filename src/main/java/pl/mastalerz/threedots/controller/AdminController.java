package pl.mastalerz.threedots.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.UserService;
import pl.mastalerz.threedots.dto.UserDto;

import java.util.List;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/admin")
@Tag(name = ApiTags.ADMIN)
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class AdminController {
    
    private final UserService userService;
    
    @GetMapping("/users")
    @Operation(summary = "Wszyscy użytkownicy")
    public ResponseEntity<List<UserDto>> allUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }
}
