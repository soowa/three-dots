package pl.mastalerz.threedots.controller.stock;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.stock.StockFacade;
import pl.mastalerz.threedots.dto.ProductStockDto;
import pl.mastalerz.threedots.dto.StockResponseDto;
import pl.mastalerz.threedots.dto.StockWithSizes;

@RestController
@RequestMapping("/api/public/stocks")
@RequiredArgsConstructor
@Tag(name = ApiTags.STOCK)
public class PublicStockController {

    private final StockFacade stockFacade;

    @Operation(summary = "Stan na magazynie")
    @GetMapping("/{id}")
    public ResponseEntity<StockResponseDto> findByStockId(@PathVariable("id") Long stockId) {
        var stockResponseDtos = stockFacade.findByStockId(stockId);
        return ResponseEntity.ok(stockResponseDtos);
    }

    @Operation(summary = "Pełny stan na magazynie dla produktu")
    @GetMapping("/product/{productId}")
    public ResponseEntity<ProductStockDto> getStockForProduct(@PathVariable Long productId) {
        return ResponseEntity.ok(stockFacade.findByProductId(productId));
    }
}
