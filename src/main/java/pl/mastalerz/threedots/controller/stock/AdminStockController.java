package pl.mastalerz.threedots.controller.stock;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.stock.StockFacade;
import pl.mastalerz.threedots.dto.StockCreateDto;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequestMapping("/api/admin/stocks")
@RequiredArgsConstructor
@Tag(name = ApiTags.ADMIN)
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class AdminStockController {
    private final StockFacade stockFacade;

    @Operation(summary = "Utworzenie stanu na magazywanie")
    @PostMapping("/create")
    public ResponseEntity<Long> create(@RequestBody StockCreateDto dto) {
        Long size = stockFacade.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).body(size);
    }

    @Operation(summary = "Aktualizacja stanu na magazynie")
    @PutMapping("/{id}/update-stock")
    public void updateStock(@PathVariable("id") Long id, @RequestParam("amount") int amount) {
        stockFacade.updateAmount(id, amount);
    }

    @Operation(summary = "Usunięsie stanu na magazynie")
    @DeleteMapping("/{id}/remove")
    public void deleteStock(@PathVariable("id") Long id) {
        stockFacade.delete(id);
    }

    @Operation(summary = "Wyczyszczenie calego magazynu ( ͡° ͜ʖ ͡°) (tak, całego! - sic!)")
    @DeleteMapping("/remove-all")
    public void deleteAllStocks() {
        stockFacade.deleteAll();
    }

    @Operation(summary = "Usunięcie stanu magazynu dla wariantu")
    @DeleteMapping("/remove-all-for-variant")
    public void deleteAllForVariant(@RequestParam("variantId") Long variantId){
        stockFacade.deleteAllForVariant(variantId);
    }
}
