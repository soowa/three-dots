package pl.mastalerz.threedots.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mastalerz.threedots.api.AccountCreationService;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.dto.LoginRequest;
import pl.mastalerz.threedots.dto.LoginResponse;
import pl.mastalerz.threedots.dto.RegisterUserDto;
import pl.mastalerz.threedots.dto.RegisterUserResult;
import pl.mastalerz.threedots.security.AuthUserDetails;
import pl.mastalerz.threedots.security.JwtUtils;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/public/auth")
@Tag(name = ApiTags.AUTH)
public class AuthController {

    private final AccountCreationService accountCreationService;
    private final AuthenticationManager authenticationManager;
    private final JwtUtils jwtUtils;

    @PostMapping("/register")
    @Operation(summary = "Rejestracja nowego użytkownika")
    public ResponseEntity<RegisterUserResult> registerUser(@RequestBody RegisterUserDto request) {
        return ResponseEntity.ok(accountCreationService.create(request));
    }

    @PostMapping("/login")
    @Operation(summary = "Logowanie nowego użytkownika")
    public ResponseEntity login(@RequestBody LoginRequest request) {
        var authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        var token = jwtUtils.generateJwtToken((AuthUserDetails) authentication.getPrincipal());

        return ResponseEntity.ok(new LoginResponse(token));
    }
}
