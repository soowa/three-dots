package pl.mastalerz.threedots.controller.product;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.product.ProductFacade;
import pl.mastalerz.threedots.dto.ReviewCreateDto;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user/products")
@Tag(name = ApiTags.PRODUCTS)
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class UserProductController {

    private final ProductFacade productFacade;

    @Operation(summary = "Dodanie opinii do produktu")
    @PostMapping("/{productId}/reviews/create")
    public ResponseEntity<?> create(@PathVariable("productId") Long id, @RequestBody ReviewCreateDto dto) {
        productFacade.addReview(id, dto);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
