package pl.mastalerz.threedots.controller.product;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.product.ProductFacade;
import pl.mastalerz.threedots.dto.DiscountDto;
import pl.mastalerz.threedots.dto.ProductCreateDto;
import pl.mastalerz.threedots.dto.VariantCreateDto;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/admin/products")
@Tags({@Tag(name = ApiTags.ADMIN), @Tag(name = ApiTags.PRODUCTS)})
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class AdminProductController {
    private final ProductFacade productFacade;

    @Operation(summary = "Utworzenie produktu")
    @PostMapping("/create")
    public ResponseEntity<String> create(@RequestBody ProductCreateDto dto) {
        String product = productFacade.create(dto);

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(product);
    }

    @Operation(summary = "Utworzenie wariantu dla produktu")
    @PostMapping("/{productId}/add")
    public ResponseEntity<Object> createVariant(@PathVariable("productId") Long productId, @RequestBody VariantCreateDto dto) {
        productFacade.createVariant(productId, dto);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Usunięcie wariantu produktu")
    @DeleteMapping("/{productId}/remove-variant/{variantId}")
    public ResponseEntity<Object> deleteVariant(@PathVariable("productId") Long productId, @PathVariable("variantId") Long variantId) {
        productFacade.deleteVariant(productId, variantId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Operation(summary = "Usunięcie wszystkich wariantów produktu ( ͡° ͜ʖ ͡°) (Oj tak, każdego! - sic!)")
    @DeleteMapping("/{variantId}/remove-all")
    public ResponseEntity<Object> deleteVariants(@PathVariable("variantId") Long variantId) {
        productFacade.deleteVariants(variantId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Operation(summary = "Dodanie zniżki dla produktu")
    @PutMapping("/{productId}/update-discount")
    public ResponseEntity<?> updateDiscount(@PathVariable("productId") Long productId, @RequestBody DiscountDto dto) {
        productFacade.updateDiscount(productId, dto);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Operation(summary = "Usunięcie produktu")
    @DeleteMapping("/{productId}/remove")
    public void delete(@PathVariable("productId") Long productId) {
        productFacade.deleteModel(productId);
    }

    @Operation(summary = "Odpięcie kategorii z produktu")
    @PutMapping("/{productId}/remove-category")
    public void removeFromCategory(@PathVariable("productId") Long productId) {
        productFacade.deleteFromCategory(productId);
    }

    @Operation(summary = "Przypisanie kategorii do produktu")
    @PutMapping("/{productId}/add-category/{categoryId}")
    public void addToCategory(@PathVariable("productId") Long productId, @PathVariable("categoryId") Long categoryId) {
        productFacade.addToCategory(productId, categoryId);
    }

    @Operation(summary = "Usunięcie opinii o produkcie")
    @DeleteMapping("/{productId}/reviews/delete/{reviewId}")
    public ResponseEntity<Object> deleteReview(@PathVariable Long productId, @PathVariable("reviewId") Long reviewId) {
        productFacade.deleteReview(productId, reviewId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Operation(summary = "Usunięcie wszystkich opinii o produkcie ( ͡° ͜ʖ ͡°) (Tak! Nic nie zostanie - sic!)")
    @DeleteMapping("/{productId}/reviews/delete/all")
    public ResponseEntity<Object> deleteReviews(@PathVariable Long productId) {
        productFacade.deleteAllReviews(productId);

        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
