package pl.mastalerz.threedots.controller.product;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.product.ProductFacade;
import pl.mastalerz.threedots.dto.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/public/products")
@RequiredArgsConstructor
@Tag(name = ApiTags.PRODUCTS)
public class ProductController {

    private final ProductFacade productFacade;

    @Operation(summary = "Wyszukiwanie wariantów (psst.. pageSize=100000.. ( ͡° ͜ʖ ͡°) )")
    @PostMapping(value = "/search")
    public ResponseEntity<ProductSearchResult> find(@RequestBody ProductQuery query) {
        return ResponseEntity.ok(productFacade.find(query));
    }

    @Operation(summary = "Szczegóły produktu")
    @GetMapping("/{productId}")
    public ResponseEntity<ProductResponseDto> findProduct(@PathVariable("productId") Long productId) {
        var product = productFacade.findProduct(productId);

        return ResponseEntity.ok(product);
    }

    @Operation(summary = "Lista produktów w kategorii")
    @GetMapping
    public ResponseEntity<List<ProductResponseDto>> findAllProductsForCategory(@RequestParam("categoryId") Long categoryId) {
        var products = productFacade.findAllProductsForCategory(categoryId);

        return ResponseEntity.ok(products);
    }

    @Operation(summary = "Wszystkie produkty")
    @GetMapping("/all")
    public ResponseEntity<List<ProductResponseDto>> findAllModels() {
        var products = productFacade.findAllModels();

        return ResponseEntity.ok(products);
    }

    @Operation(summary = "Wszystkie warianty dla produktu")
    @GetMapping("/{productId}/variants")
    public ResponseEntity<ProductAndVariantsResponseDto> findByIdAllVariants(@PathVariable("productId") Long productId) {
        var products = productFacade.findByIdAllVariants(productId);

        return ResponseEntity.ok(products);
    }

    @Operation(summary = "Opinie o produkcie")
    @GetMapping("/{productId}/reviews")
    public ResponseEntity<?> getAllReviews(@PathVariable Long productId) {
        return ResponseEntity.ok(productFacade.getAllReviews(productId));
    }
}
