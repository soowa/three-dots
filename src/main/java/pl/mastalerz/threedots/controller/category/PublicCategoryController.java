package pl.mastalerz.threedots.controller.category;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.category.CategoryFacade;
import pl.mastalerz.threedots.dto.CategoryResponseDto;

import java.util.List;

@RequestMapping("/api/public/categories")
@RestController
@RequiredArgsConstructor
@Tag(name = ApiTags.CATEGORIES)
public class PublicCategoryController {

    private final CategoryFacade categoryFacade;

    @Operation(summary = "Wszystkie kategorie")
    @GetMapping
    public ResponseEntity<List<CategoryResponseDto>> findAllCategories() {
        var allCategories = categoryFacade.findAllCategories();

        return ResponseEntity.ok(allCategories);
    }
}
