package pl.mastalerz.threedots.controller.category;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.category.CategoryFacade;
import pl.mastalerz.threedots.dto.CategoryCreateDto;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;

@RestController
@RequestMapping("/api/admin/categories")
@RequiredArgsConstructor
@Tags({@Tag(name = ApiTags.ADMIN), @Tag(name = ApiTags.CATEGORIES)})
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class AdminCategoryController {

    private final CategoryFacade categoryFacade;

    @Operation(summary = "Utworzenie kategorii")
    @PostMapping("/create")
    public ResponseEntity<Long> createCategory(@RequestBody CategoryCreateDto dto) {
        Long category = categoryFacade.createCategory(dto);

        return ResponseEntity.status(HttpStatus.CREATED)
                .body(category);
    }

    @Operation(summary = "Zmiana nazwy kategorii")
    @PutMapping("/{categoryId}")
    public ResponseEntity<Object> changeCategoryName(@PathVariable("categoryId") Long categoryId, @RequestParam("name") String name) {
        categoryFacade.changeCategoryName(categoryId, name);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Operation(summary = "Usunięcie kategorii")
    @DeleteMapping("/{categoryId}")
    public void deleteCategory(@PathVariable("categoryId") Long categoryId) {
        categoryFacade.deleteCategory(categoryId);
    }
}
