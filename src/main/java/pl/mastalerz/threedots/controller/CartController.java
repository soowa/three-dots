package pl.mastalerz.threedots.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mastalerz.threedots.api.ApiTags;
import pl.mastalerz.threedots.api.cart.CartFacade;
import pl.mastalerz.threedots.domain.user.Nick;
import pl.mastalerz.threedots.dto.CartAddDto;
import pl.mastalerz.threedots.dto.CartChangeItemAmountDto;
import pl.mastalerz.threedots.dto.CartResponse;
import pl.mastalerz.threedots.security.UserContextHelper;

import static pl.mastalerz.threedots.OpenApiConfig.SECURITY_SCHEMA_NAME;
import static pl.mastalerz.threedots.security.UserContextHelper.*;

@RestController
@RequestMapping("/api/cart")
@RequiredArgsConstructor
@Tag(name = ApiTags.CART)
@SecurityRequirement(name = SECURITY_SCHEMA_NAME)
public class CartController {
    private final CartFacade facade;

    @GetMapping("/refresh")
    @Operation(summary = "Stan koszyka")
    public ResponseEntity<CartResponse> getCurrentCart() {
        final var nickname = currentUser().getNickname();
        final var response = facade.getCart(Nick.of(nickname));
        return ResponseEntity.ok(response);
    }

    @PutMapping("/add")
    @Operation(summary = "Dodanie produktu do koszyka")
    public ResponseEntity<?> addProduct(@RequestBody CartAddDto cartAddDto) {
        facade.addProduct(currentUser(), cartAddDto);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/remove/{variantId}")
    @Operation(summary = "Usunięcie produktu z koszyka")
    public ResponseEntity<?> removeProduct(@PathVariable("variantId") Long variantId) {
        facade.removeProduct(currentUser(), variantId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/update-amount")
    @Operation(summary = "Aktualizacja ilości produktu w koszyku")
    public ResponseEntity<?> updateProductAmount(@RequestBody CartChangeItemAmountDto itemAmountDto) {
        facade.updateItemAmount(currentUser(), itemAmountDto);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/decrease/{variantId}")
    @Operation(summary = "Pomniejszenie ilości produktu o 1")
    public ResponseEntity<?> decreaseByOne(@PathVariable("variantId") Long variantId) {
        facade.decreaseByOne(currentUser(), variantId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/increase/{variantId}")
    @Operation(summary = "Zwiększenie ilości produktu o 1")
    public ResponseEntity<?> increaseByOne(@PathVariable("variantId") Long variantId) {
        facade.increaseByOne(currentUser(), variantId);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/finalize")
    @Operation(summary = "Finalizacja koszyka", description = "Powoduje utworzenie nowego zamówienia i wyczyszczenie koszyka klienta")
    public ResponseEntity<?> finalizeCart() {
        facade.transformToOrder(currentUser());
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
