package pl.mastalerz.threedots.exception;

import lombok.Getter;

@Getter
public class DoesNotExistException extends RuntimeException {
    private final int HTTP_STATUS = 404;
    private final String HTTP_ERROR_CODE = "RESOURCE_DOES_NOT_EXIST";

    public DoesNotExistException(String message) {
        super(message);
    }

    public DoesNotExistException(String message, Throwable cause) {
        super(message, cause);
    }
}
