package pl.mastalerz.threedots.exception;

import org.aspectj.weaver.patterns.FormalBinding;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<List<ExceptionResponse>> handleValidationException(ValidationException exception){
        List<ExceptionResponse> errors = exception.getErrors()
                .stream()
                .map( e -> ExceptionResponse.builder()
                        .message(e.getMessage())
                        .errorCode(e.getCode())
                        .build())
                .toList();

        return ResponseEntity
                .status(exception.getHttpErrorCode())
                .body(errors);
    }

    @ExceptionHandler(DoesNotExistException.class)
    public ResponseEntity<ExceptionResponse> handleResourceFoundException(DoesNotExistException exception){
        final var httpStatus = exception.getHTTP_STATUS();
        final var httpErrorCode = exception.getHTTP_ERROR_CODE();
        final var message = exception.getMessage();

        final var error = ExceptionResponse.builder()
                .errorCode(httpErrorCode)
                .message(message)
                .build();

        return ResponseEntity
                .status(httpStatus)
                .body(error);
    }

    @ExceptionHandler(ForbiddenAccessException.class)
    public ResponseEntity<ExceptionResponse> handleForbiddenAccess(ForbiddenAccessException ex) {
        var response = ExceptionResponse.builder()
                .message("User %d cannot access resource %s".formatted(ex.getDeniedUserId(), ex.getResourceName()))
                .build();

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
    }
}
