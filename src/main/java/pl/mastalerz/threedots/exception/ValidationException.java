package pl.mastalerz.threedots.exception;

import lombok.Getter;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.util.List;

@Getter
public class ValidationException extends RuntimeException{
    private static final int HTTP_ERROR_CODE = 400;
    private final List<ValidationError> errors;

    public ValidationException(ValidationError error) {
        this.errors = List.of(error);
    }

    public ValidationException(List<ValidationError> errors) {
        this.errors = errors;
    }

    public int getHttpErrorCode(){
        return HTTP_ERROR_CODE;
    }
}
