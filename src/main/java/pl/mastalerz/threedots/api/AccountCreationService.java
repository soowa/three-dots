package pl.mastalerz.threedots.api;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.api.validator.user.RegisterUserDtoValidator;
import pl.mastalerz.threedots.domain.address.*;
import pl.mastalerz.threedots.domain.user.*;
import pl.mastalerz.threedots.dto.RegisterUserDto;
import pl.mastalerz.threedots.dto.RegisterUserResult;
import pl.mastalerz.threedots.exception.ValidationException;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional
public class AccountCreationService {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final RegisterUserDtoValidator validator;

    public RegisterUserResult create(RegisterUserDto user) {
        throwValidationExceptionIfErrorsExists(validator.validate(user));
        final var newUser = userRepository.save(doCreate(user));

        return new RegisterUserResult(newUser.getId());
    }

    private User doCreate(RegisterUserDto user) {
        var password = encoder.encode(user.getPassword());

        return User.create(
                Nick.of(user.getUsername()),
                Email.of(user.getEmail()),
                password,
                FirstName.of(user.getFirstName()),
                LastName.of(user.getLastName()),
                PhoneNumber.of(user.getPhoneNumber()),
                new Address(
                        City.of(user.getCity()),
                        ZipCode.of(user.getZipCode()),
                        Street.of(user.getStreet()),
                        BuildingNumber.of(user.getBuildingNumber()),
                        ApartmentNumber.of(user.getApartmentNumber())
                )
        );
    }


    private void throwValidationExceptionIfErrorsExists(List<ValidationError> errors) {
        if (!errors.isEmpty())
            throw new ValidationException(errors);
    }
}
