package pl.mastalerz.threedots.api;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mastalerz.threedots.domain.user.UserRepository;
import pl.mastalerz.threedots.domain.user.UserRole;
import pl.mastalerz.threedots.dto.UserDto;
import pl.mastalerz.threedots.dto.UserMapper;

import java.util.List;
import java.util.stream.Collectors;

import static pl.mastalerz.threedots.domain.user.UserRole.*;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository users;

    public List<UserDto> getAllUsers() {
        return users.findAllByRole(CLIENT).stream()
                .map(UserMapper::toUserDto)
                .collect(Collectors.toList());
    }
}
