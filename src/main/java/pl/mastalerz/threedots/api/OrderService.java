package pl.mastalerz.threedots.api;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mastalerz.threedots.domain.order.OrderRepository;
import pl.mastalerz.threedots.domain.user.UserRole;
import pl.mastalerz.threedots.dto.OrderDto;
import pl.mastalerz.threedots.dto.OrderDtoMapper;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.exception.ForbiddenAccessException;
import pl.mastalerz.threedots.security.UserContextHelper;

import java.util.List;
import java.util.stream.Collectors;

import static pl.mastalerz.threedots.security.UserContextHelper.*;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orders;

    public OrderDto getOrder(Long orderId) {
        return orders.findById(orderId)
                .map(OrderDtoMapper::map)
                .orElseThrow(() -> new DoesNotExistException("No order for id " + orderId));
    }

    public List<OrderDto> getActiveOrdersForUser(Long userId) {
        if (currentUser().getRole() == UserRole.CLIENT && !currentUser().getUserId().equals(userId)) {
            throw new ForbiddenAccessException(currentUser().getUserId(), "Orders");
        };

        return orders.findActiveForOwner(userId).stream()
                .map(OrderDtoMapper::map)
                .collect(Collectors.toList());
    }

    public List<OrderDto> getAllOrdersForUser(Long userId) {
        if (currentUser().getRole() == UserRole.CLIENT && !currentUser().getUserId().equals(userId)) {
            throw new ForbiddenAccessException(currentUser().getUserId(), "Orders");
        };

        return orders.findByOwnerId(userId).stream()
                .map(OrderDtoMapper::map)
                .collect(Collectors.toList());
    }
}
