package pl.mastalerz.threedots.api;

public class ApiTags {

    private ApiTags() {
    }

    public static final String PRODUCTS = "Products";
    public static final String CATEGORIES = "Categories";
    public static final String STOCK = "Stock";
    public static final String ORDER = "Order";
    public static final String ADMIN = "Admin";
    public static final String CART = "Cart";
    public static final String SIZE = "Size";
    public static final String AUTH = "Auth";

}
