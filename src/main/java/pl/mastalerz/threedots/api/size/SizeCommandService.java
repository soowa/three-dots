package pl.mastalerz.threedots.api.size;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.api.validator.size.SizeCreateValidator;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.size.SizeLabel;
import pl.mastalerz.threedots.domain.size.SizeRepository;
import pl.mastalerz.threedots.dto.SizeCreateDto;
import pl.mastalerz.threedots.exception.ValidationException;

import java.util.List;

@Service
@RequiredArgsConstructor
class SizeCommandService {
    private final SizeRepository sizeRepository;
    private final SizeCreateValidator sizeCreateValidator;

    public Long create(SizeCreateDto dto){
        var sizeErrors = sizeCreateValidator.validate(dto);
        this.throwValidationExceptionIfErrorsExists(sizeErrors);

        final var size = createSize(dto);
        final var saved = sizeRepository.save(size);
        return saved.getId();
    }

    public void delete(Long id){
        sizeRepository.deleteById(id);
    }

    public void deleteAll(){
        sizeRepository.deleteAll();
    }

    private Size createSize(SizeCreateDto dto){
        final var sizeLabel = SizeLabel.of(dto.getSizeLabel());
        return Size.builder()
                .sizeLabel(sizeLabel)
                .build();
    }

    private void throwValidationExceptionIfErrorsExists(List<ValidationError> errors) {
        if (!errors.isEmpty()) throw new ValidationException(errors);
    }
}
