package pl.mastalerz.threedots.api.size;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.api.validator.size.SizeCreateValidator;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.size.SizeLabel;
import pl.mastalerz.threedots.domain.size.SizeRepository;
import pl.mastalerz.threedots.dto.ProductMapper;
import pl.mastalerz.threedots.dto.SizeCreateDto;
import pl.mastalerz.threedots.dto.SizeMapper;
import pl.mastalerz.threedots.dto.SizeResponseDto;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.exception.ValidationException;

import java.util.List;

@Service
@RequiredArgsConstructor
class SizeQueryService {
    private final SizeRepository sizeRepository;

    public SizeResponseDto findById(Long id){
        return sizeRepository.findById(id)
                .map(SizeMapper::toSizeResponseDto)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego rozmiaru"));
    }

    public List<SizeResponseDto> findAll(){
        return sizeRepository.findAll()
                .stream()
                .map(SizeMapper::toSizeResponseDto)
                .toList();
    }
}
