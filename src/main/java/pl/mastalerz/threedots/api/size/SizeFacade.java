package pl.mastalerz.threedots.api.size;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.dto.SizeCreateDto;
import pl.mastalerz.threedots.dto.SizeMapper;
import pl.mastalerz.threedots.dto.SizeResponseDto;

import java.util.List;

@Component
@RequiredArgsConstructor
public class SizeFacade {
    private final SizeCommandService sizeCommandService;
    private final SizeQueryService sizeQueryService;

    public Long create(SizeCreateDto dto){
        return sizeCommandService.create(dto);
    }

    public void delete(Long id){
        sizeCommandService.delete(id);
    }

    public void deleteAll(){
        sizeCommandService.deleteAll();
    }

    public SizeResponseDto findById(Long id){
        return this.sizeQueryService.findById(id);
    }

    public List<SizeResponseDto> findAll(){
        return this.sizeQueryService.findAll();
    }

}
