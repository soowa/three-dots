package pl.mastalerz.threedots.api;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mastalerz.threedots.domain.address.Address;
import pl.mastalerz.threedots.domain.order.*;
import pl.mastalerz.threedots.domain.product.Cart;
import pl.mastalerz.threedots.domain.product.CartItem;
import pl.mastalerz.threedots.domain.product.CartRepository;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.stock.Stock;
import pl.mastalerz.threedots.domain.stock.StockAmount;
import pl.mastalerz.threedots.domain.stock.StockRepository;
import pl.mastalerz.threedots.domain.user.Nick;
import pl.mastalerz.threedots.domain.user.User;
import pl.mastalerz.threedots.domain.user.UserRepository;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.exception.ValidationException;
import pl.mastalerz.threedots.security.AuthUserDetails;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional
public class TransformCartService {

    private final OrderRepository orderRepository;
    private final CartRepository cartRepository;
    private final StockRepository stockRepository;
    private final UserRepository userRepository;

    public OrderNumber transform(AuthUserDetails user) {
        final var cart = loadCart(user);
        validateEmptyCart(cart);
        validateCartWithCurrentStock(cart);
        final var newOrder = createOrder(loadUser(user), cart);
        substractStock(cart);
        cartRepository.delete(cart);
        return orderRepository.save(newOrder).getNumber();
    }

    private Order createOrder(User user, Cart cart) {
        final var orderAddress = createOrderAddress(user.getAddress());
        final var orderItems = cart.getItems()
                .stream()
                .map(this::createOrderItem)
                .toList();

        return Order.createNew(orderAddress, orderItems, user);
    }

    private OrderItem createOrderItem(CartItem item) {
        return new OrderItem(
                item.getProduct().getName().getValue(),
                item.getVariant().getVariantNumber(),
                item.getAmount(),
                item.getSize().getSizeLabel().getValue(),
                item.getVariant().getColor().getValue(),
                item.getPrice(),
                item.getPrice().multiply(item.getAmount())
        );
    }

    private OrderAddress createOrderAddress(Address address) {
        return new OrderAddress(
                address.getCity(),
                address.getZipCode(),
                address.getStreet(),
                address.getBuildingNumber(),
                address.getApartmentNumber()
        );
    }

    private void validateCartWithCurrentStock(Cart cart) {
        for (var item : cart.getItems()) {
            final var hasEnough = validateCartItemStock(item);
            if (!hasEnough) {
                throw new ValidationException(ApiError.NOT_ENOUGH_IN_STOCK);
            }
        }
    }

    private void substractStock(Cart cart){
        for (var item : cart.getItems()) {
            final var stockItem = stockRepository
                    .findByVariantIdAndSize(item.getVariant().getId(), item.getSize())
                    .get();
            final var newStock = stockItem.getAmount().substract(StockAmount.of(item.getAmount()));
            if (newStock.notZero()){
                stockItem.updateStockAmount(newStock);
            } else {
                stockRepository.delete(stockItem);
            }
        }
    }

    private void validateEmptyCart(Cart cart) {
        if (cart.getItems().isEmpty())
            throw new ValidationException(ApiError.EMPTY_CART);
    }

    private boolean validateCartItemStock(CartItem item) {
        final var itemStock = loadStock(item);
        return itemStock.hasEnough(item.getAmount());
    }

    private User loadUser(AuthUserDetails user) {
        return userRepository.findByNick(Nick.of(user.getNickname()))
                .orElseThrow(() -> new DoesNotExistException("No cart found for user " + user.getNickname()));
    }

    private Cart loadCart(AuthUserDetails user) {
        return cartRepository.findCartByUser_Nick(Nick.of(user.getNickname()))
                .orElseThrow(() -> new DoesNotExistException("No cart found for user " + user.getNickname()));
    }

    private Stock loadStock(CartItem cartItem) {
        final var variantNumber = cartItem.getVariant();
        final var size = cartItem.getSize();
        return loadStock(variantNumber.getVariantNumber(), size);
    }

    private Stock loadStock(UUID variantNumber, Size size) {
        return stockRepository.findByVariant_VariantNumberAndSize(variantNumber, size)
                .orElseThrow(() -> new DoesNotExistException("No stock found for variant " + variantNumber + " for size " + size));
    }
}
