package pl.mastalerz.threedots.api.validator.stock;

import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.util.ArrayList;
import java.util.List;

@Component
class StockAmountValidator {
    private enum Error implements ValidationError {
        AMOUNT_LESS_TEN_ZERO("Ilość nie może być mniejsza niż zero");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(int value) {
        List<ValidationError> errors = new ArrayList<>();

        if (isStockLessThanZero(value))
            errors.add(Error.AMOUNT_LESS_TEN_ZERO);

        return errors;
    }

    private boolean isStockLessThanZero(int value) {
        return value < 0;
    }
}
