package pl.mastalerz.threedots.api.validator.product;

import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
class PriceValidator {

    private enum Error implements ValidationError {
        PRICE_LESS_THAN_ZERO("Cena nie może być mniejsza niż zero");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(BigDecimal value) {
        List<ValidationError> errors = new ArrayList<>();

        if (!isPriceLessThanZero(value))
            errors.add(Error.PRICE_LESS_THAN_ZERO);

        return errors;
    }

    private boolean isPriceLessThanZero(BigDecimal value) {
        return BigDecimal.ZERO.compareTo(value) <= 0;
    }
}
