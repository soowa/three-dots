package pl.mastalerz.threedots.api.validator.review;

import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.util.ArrayList;
import java.util.List;

@Component
class ReviewRatingValidator {
    private enum Error implements ValidationError {
        INCORRECT_BLANK("Kontent nie może być pusty"),
        NOT_IN_RANGE("Ocena musi być w zakresie od 1 do 4");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(Integer value) {
        List<ValidationError> errors = new ArrayList<>();

        if (value == null) {
            errors.add(Error.INCORRECT_BLANK);
            return errors;
        }

        if (!isRatingInScope(value))
            errors.add(Error.NOT_IN_RANGE);

        return errors;
    }

    private boolean isRatingInScope(int value){
        return value >= 1 && value <= 4;
    }
}
