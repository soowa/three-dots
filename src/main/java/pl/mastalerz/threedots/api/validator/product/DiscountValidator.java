package pl.mastalerz.threedots.api.validator.product;

import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
class DiscountValidator {
    private enum Error implements ValidationError{
        DISCOUNT_LESS_THAN_ZERO("Zniżka nie może być mniejsza niż zero");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(BigDecimal value) {
        List<ValidationError> errors = new ArrayList<>();

        if (isDiscountLessThanZero(value))
            errors.add(Error.DISCOUNT_LESS_THAN_ZERO);

        return errors;
    }

    private boolean isDiscountLessThanZero(BigDecimal value) {
        return BigDecimal.ZERO.compareTo(value) > 0;
    }

}
