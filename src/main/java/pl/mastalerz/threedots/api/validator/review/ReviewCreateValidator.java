package pl.mastalerz.threedots.api.validator.review;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.dto.ReviewCreateDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ReviewCreateValidator {
    private final ReviewContentValidator reviewContentValidator;
    private final ReviewRatingValidator reviewRatingValidator;

    public List<ValidationError> validate(ReviewCreateDto dto){
        List<ValidationError> errors = new ArrayList<>();

        var contentErrors = reviewContentValidator.validate(dto.getContent());
        errors.addAll(contentErrors);

        var ratingErrors = reviewRatingValidator.validate(dto.getRating());
        errors.addAll(ratingErrors);

        return errors;
    }

}
