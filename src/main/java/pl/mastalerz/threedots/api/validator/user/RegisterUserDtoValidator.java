package pl.mastalerz.threedots.api.validator.user;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.domain.user.UserRepository;
import pl.mastalerz.threedots.dto.RegisterUserDto;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
@RequiredArgsConstructor
public class RegisterUserDtoValidator {
    private final UserRepository userRepository;
    private static final String PHONE_NUMBER_REGEX = "^\\d{10}$";
    private static final String ZIP_CODE_REGEX = "^([0-9]{2})(-[0-9]{3})?$";

    private enum Error implements ValidationError {
        NAME_ALREADY_USED("Nickname już użyty"),
        EMAIL_ALREADY_USED("Email już użyty"),
        NAME_CANNOT_BE_EMPTY("Nazwa nie może być pusta"),
        EMAIL_CANNOT_BE_EMPTY("Email nie może być pusty"),
        FIRST_NAME_CANNOT_BE_EMPTY("Imie nie może być puste"),
        LAST_NAME_CANNOT_BE_EMPTY("Nazwisko nie może być puste"),
        PASSWORD_CANNOT_BE_EMPTY("Haslo nie może być puste"),
        PHONE_NUMBER_CANNOT_BE_EMPTY("Numer telefonu jest wymagany"),
        PHONE_NUMBER_DOES_NOT_MATCH("Numer telefonu musi miec 10 cyfer"),
        ZIP_CODE_DOES_NOT_MATCH("Kod pocztowy musi prasowac do wzoru XX-XXX"),
        ZIP_CODE_CANNOT_BE_EMPTY("Kod pocztowy nie może być pusty"),
        CITY_CANNOT_BE_EMPTY("Miasto jest wymagane"),
        STREET_CANNOT_BE_EMPTY("Ulica jest wymagane"),
        APARTMENT_NUMBER_CANNOT_BE_EMPTY("Numer mieszkania jest wymagany"),
        BUILDING_NUMBER_CANNOT_BE_EMPTY("Numer budynku jest wymagany");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(RegisterUserDto dto) {
        List<ValidationError> errors = new ArrayList<>();

        if (isNameUsed(dto.getUsername()))
            errors.add(Error.NAME_ALREADY_USED);

        if (isBlank(dto.getUsername()))
            errors.add(Error.NAME_CANNOT_BE_EMPTY);

        if (isEmailUsed(dto.getEmail()))
            errors.add(Error.EMAIL_ALREADY_USED);

        if (isBlank(dto.getEmail()))
            errors.add(Error.EMAIL_CANNOT_BE_EMPTY);

        if (isBlank(dto.getFirstName()))
            errors.add(Error.FIRST_NAME_CANNOT_BE_EMPTY);

        if (isBlank(dto.getLastName()))
            errors.add(Error.LAST_NAME_CANNOT_BE_EMPTY);

        if (isBlank(dto.getPassword()))
            errors.add(Error.PASSWORD_CANNOT_BE_EMPTY);

        if (!isPhoneNumberMatch(dto.getPhoneNumber()))
            errors.add(Error.PHONE_NUMBER_DOES_NOT_MATCH);

        if (isBlank(dto.getPhoneNumber()))
            errors.add(Error.PHONE_NUMBER_CANNOT_BE_EMPTY);

        if (!isZipCodeMatch(dto.getZipCode()))
            errors.add(Error.ZIP_CODE_DOES_NOT_MATCH);

        if (isBlank(dto.getZipCode()))
            errors.add(Error.ZIP_CODE_CANNOT_BE_EMPTY);

        if (isBlank(dto.getCity()))
            errors.add(Error.CITY_CANNOT_BE_EMPTY);

        if (isBlank(dto.getStreet()))
            errors.add(Error.STREET_CANNOT_BE_EMPTY);

        if (isBlank(dto.getApartmentNumber()))
            errors.add(Error.APARTMENT_NUMBER_CANNOT_BE_EMPTY);

        if (isBlank(dto.getBuildingNumber()))
            errors.add(Error.BUILDING_NUMBER_CANNOT_BE_EMPTY);

        return errors;
    }

    private boolean isNameUsed(String value){
        return userRepository.isNickAlreadyUsed(value);
    }

    private boolean isEmailUsed(String value){
        return userRepository.isEmailAlreadyUsed(value);
    }

    private boolean isBlank(String value){
        return StringUtils.isBlank(value);
    }

    private boolean isPhoneNumberMatch(String value){
        return Pattern.compile(PHONE_NUMBER_REGEX)
                .matcher(value)
                .matches();
    }

    private boolean isZipCodeMatch(String value){
        return Pattern.compile(ZIP_CODE_REGEX)
                .matcher(value)
                .matches();
    }
}
