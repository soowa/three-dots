package pl.mastalerz.threedots.api.validator.product;

import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
class DiscountExpirationDateValidator {
    private enum Error implements ValidationError {
        DATE_LESS_THEN_CURRENT("Data wygasniecia nie może byc mniejsza niż aktualna");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(LocalDateTime value) {
        List<ValidationError> errors = new ArrayList<>();

        if (value == null)
            return errors;

        if (!isDateAfterToday(value))
            errors.add(Error.DATE_LESS_THEN_CURRENT);

        return errors;
    }

    private boolean isDateAfterToday(LocalDateTime value) {
        return value.isAfter(LocalDateTime.now());
    }

}
