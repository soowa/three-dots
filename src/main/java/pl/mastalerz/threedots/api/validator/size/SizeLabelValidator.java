package pl.mastalerz.threedots.api.validator.size;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.domain.size.SizeRepository;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
class SizeLabelValidator {
    private final SizeRepository sizeRepository;

    private enum Error implements ValidationError {
        SIZE_BLANK("Wartość rozmiaru nie może być pusta"),
        SIZE_ALREADY_USED("Wartość jest już użyta"),
        SIZE_TOO_LONG("Wartość rozmiaru moze zawierać maksymalnie 2 litery");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(String value) {
        List<ValidationError> errors = new ArrayList<>();

        if (isNameBlank(value))
            errors.add(Error.SIZE_BLANK);

        if (isNameTooLong(value))
            errors.add(Error.SIZE_TOO_LONG);

        if (isAlreadyUsed(value))
            errors.add(Error.SIZE_ALREADY_USED);

        return errors;
    }

    private boolean isNameBlank(String value){
        return StringUtils.isBlank(value);
    }

    private boolean isNameTooLong(String value){
        return value.length() > 2;
    }

    private boolean isAlreadyUsed(String value){
        return sizeRepository.existsBySizeLabel_Value(value);
    }
}
