package pl.mastalerz.threedots.api.validator.stock;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.domain.stock.StockRepository;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
class StockSizeAlreadyUsedValidator {
    private final StockRepository stockRepository;

    private enum Error implements ValidationError {
        SIZE_ALREADY_USED("Już istnieje wartość dla podanego rozmiaru");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(Long sizeId, Long variantId) {
        List<ValidationError> errors = new ArrayList<>();

        if (sizeAlreadyUsed(sizeId, variantId))
            errors.add(Error.SIZE_ALREADY_USED);

        return errors;
    }

    private boolean sizeAlreadyUsed(Long sizeId, Long variantId) {
        return stockRepository.sizeAlreadyExistsForVariant(sizeId, variantId);
    }

}
