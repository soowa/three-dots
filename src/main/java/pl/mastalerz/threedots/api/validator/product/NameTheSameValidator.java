package pl.mastalerz.threedots.api.validator.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.domain.product.ProductRepository;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
class NameTheSameValidator {
    private final ProductRepository productRepository;

    private enum Error implements ValidationError {
        MODEL_NAME_ALREADY_USED("Nazwa modelu już użyta");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(String value) {
        List<ValidationError> errors = new ArrayList<>();

        if (isNameUsed(value))
            errors.add(Error.MODEL_NAME_ALREADY_USED);

        return errors;
    }

    private boolean isNameUsed(String value){
        return productRepository.existsByName_Value(value);
    }
}
