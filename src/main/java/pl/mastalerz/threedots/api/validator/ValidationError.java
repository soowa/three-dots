package pl.mastalerz.threedots.api.validator;

public interface ValidationError {
    String getMessage();
    String getCode();

}
