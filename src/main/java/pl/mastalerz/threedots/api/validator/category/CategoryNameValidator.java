package pl.mastalerz.threedots.api.validator.category;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.util.ArrayList;
import java.util.List;

@Component
class CategoryNameValidator {
    private enum Error implements ValidationError {
        NAME_BLANK("Name cannot be blank"),
        NAME_TOO_LONG("Name cannot be longer than 20 letters");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(String value) {
        List<ValidationError> errors = new ArrayList<>();

        if (isNameBlank(value))
            errors.add(Error.NAME_BLANK);

        if (isNameTooLong(value))
            errors.add(Error.NAME_TOO_LONG);

        return errors;
    }

    private boolean isNameBlank(String value){
        return StringUtils.isBlank(value);
    }

    private boolean isNameTooLong(String value){
        return value.length() >= 20;
    }
}
