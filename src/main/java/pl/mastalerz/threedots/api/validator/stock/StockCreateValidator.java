package pl.mastalerz.threedots.api.validator.stock;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.dto.StockCreateDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class StockCreateValidator {
    private final StockAmountValidator stockAmountValidator;
    private final StockSizeAlreadyUsedValidator stockSizeAlreadyUsedValidator;

    public List<ValidationError> validate(StockCreateDto dto){
        List<ValidationError> errors = new ArrayList<>();

        var stockError = stockAmountValidator.validate(dto.getAmount());
        errors.addAll(stockError);

        var stockSizeErrors = stockSizeAlreadyUsedValidator.validate(dto.getSizeId(), dto.getVariantId());
        errors.addAll(stockSizeErrors);

        return errors;
    }
}
