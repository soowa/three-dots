package pl.mastalerz.threedots.api.validator.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.dto.DiscountDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DiscountUpdateValidator {
    private final DiscountValidator discountValidator;
    private final DiscountExpirationDateValidator expirationDateValidator;

    public List<ValidationError> validate(DiscountDto dto){
        List<ValidationError> errors = new ArrayList<>();

        var discountErrors = discountValidator.validate(dto.getDiscount());
        errors.addAll(discountErrors);

        var dateError = expirationDateValidator.validate(dto.getDiscountExpirationDate());
        errors.addAll(dateError);

        return errors;
    }

}
