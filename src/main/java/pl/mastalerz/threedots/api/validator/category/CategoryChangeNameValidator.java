package pl.mastalerz.threedots.api.validator.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CategoryChangeNameValidator {
    private final CategoryNameValidator nameValidator;
    private final CategoryNameExistsValidator nameExistsValidator;

    public List<ValidationError> validate(String name){
        List<ValidationError> errors = new ArrayList<>();

        var nameErrors = nameValidator.validate(name);
        errors.addAll(nameErrors);

        var nameExistsError = nameExistsValidator.validate(name);
        errors.addAll(nameExistsError);

        return errors;
    }
}
