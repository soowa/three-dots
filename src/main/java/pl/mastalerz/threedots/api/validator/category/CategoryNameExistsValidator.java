package pl.mastalerz.threedots.api.validator.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.domain.category.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CategoryNameExistsValidator {

    private final CategoryRepository repository;

    private enum Error implements ValidationError {
        NAME_ALREADY_EXISTS("Name already exists");

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(String value) {
        List<ValidationError> errors = new ArrayList<>();

        if (doesTheGivenCategoryExist(value))
            errors.add(Error.NAME_ALREADY_EXISTS);

        return errors;
    }

    private boolean doesTheGivenCategoryExist(String name) {
        return repository.existsByName_Value(name);
    }

}
