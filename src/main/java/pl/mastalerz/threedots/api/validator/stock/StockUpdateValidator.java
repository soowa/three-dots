package pl.mastalerz.threedots.api.validator.stock;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class StockUpdateValidator {
    private final StockAmountValidator stockAmountValidator;

    public List<ValidationError> validate(int value){
        List<ValidationError> errors = new ArrayList<>();

        var stockError = stockAmountValidator.validate(value);
        errors.addAll(stockError);

        return errors;
    }
}
