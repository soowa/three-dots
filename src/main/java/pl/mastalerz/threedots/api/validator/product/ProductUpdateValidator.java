package pl.mastalerz.threedots.api.validator.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.api.validator.stock.StockCreateValidator;
import pl.mastalerz.threedots.dto.ProductUpdateDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ProductUpdateValidator {
    private final StockCreateValidator stockChangeValidator;
    private final DiscountValidator discountValidator;
    private final DiscountExpirationDateValidator expirationDateValidator;
    private final PriceValidator priceValidator;

    public List<ValidationError> validate(ProductUpdateDto dto){
        List<ValidationError> errors = new ArrayList<>();

        if (dto.getPrice() != null){
            var priceErrors = priceValidator.validate(dto.getPrice());
            errors.addAll(priceErrors);
        }

        if (dto.getDiscount() != null && dto.getDiscountExpirationDate() != null){
            var discountErrors = discountValidator.validate(dto.getDiscount());
            errors.addAll(discountErrors);

            var dateError = expirationDateValidator.validate(dto.getDiscountExpirationDate());
            errors.addAll(dateError);
        }

//        var stockErrors = stockChangeValidator.validate(dto.getStock());
//        errors.addAll(stockErrors);

        return errors;
    }

}
