package pl.mastalerz.threedots.api.validator.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.dto.CategoryCreateDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CategoryCreateValidator {
    private final CategoryNameValidator nameValidator;
    private final CategoryNameExistsValidator nameExistsValidator;

    public List<ValidationError> validate(CategoryCreateDto dto){
        List<ValidationError> errors = new ArrayList<>();

        var nameErrors = nameValidator.validate(dto.getName());
        errors.addAll(nameErrors);

        var nameExistsError = nameExistsValidator.validate(dto.getName());
        errors.addAll(nameExistsError);

        return errors;
    }
}
