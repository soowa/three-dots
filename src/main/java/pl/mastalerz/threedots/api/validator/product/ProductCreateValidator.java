package pl.mastalerz.threedots.api.validator.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.dto.ProductCreateDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ProductCreateValidator {
    private final NameValidator nameValidator;
    private final PriceValidator priceValidator;
    private final DescriptionValidator descriptionValidator;
    private final NameTheSameValidator nameTheSameValidator;
    private final DiscountValidator discountValidator;
    private final DiscountExpirationDateValidator expirationDateValidator;

    public List<ValidationError> validate(ProductCreateDto dto) {
        List<ValidationError> errors = new ArrayList<>();

        var nameErrors = nameValidator.validate(dto.getName());
        errors.addAll(nameErrors);

        var nameNotTheSameErrors = nameTheSameValidator.validate(dto.getName());
        errors.addAll(nameNotTheSameErrors);

        var priceErrors = priceValidator.validate(dto.getPrice());
        errors.addAll(priceErrors);

        if (dto.getDescription() != null) {
            var descriptionErrors = descriptionValidator.validate(dto.getDescription());
            errors.addAll(descriptionErrors);
        }

        if (dto.getDiscount() != null) {
            var discountErrors = discountValidator.validate(dto.getDiscount());
            errors.addAll(discountErrors);
        }

        if (dto.getDiscountExpirationDate() != null) {
            var discountExpirationDateErrors = expirationDateValidator.validate(dto.getDiscountExpirationDate());
            errors.addAll(discountExpirationDateErrors);
        }
        return errors;
    }
}
