package pl.mastalerz.threedots.api.validator.variant;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.dto.VariantCreateDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class VariantCreateValidator {
    private final VariantColorValidator colorValidator;

    public List<ValidationError> validate(VariantCreateDto dto) {
        List<ValidationError> errors = new ArrayList<>();

        var colorErrors = colorValidator.validate(dto.getColor());
        errors.addAll(colorErrors);

        return errors;
    }

}
