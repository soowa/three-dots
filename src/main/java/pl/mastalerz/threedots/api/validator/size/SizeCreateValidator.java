package pl.mastalerz.threedots.api.validator.size;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.dto.SizeCreateDto;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class SizeCreateValidator {
    private final SizeLabelValidator sizeLabelValidator;

    public List<ValidationError> validate(SizeCreateDto dto){
        List<ValidationError> errors = new ArrayList<>();

        var stockError = sizeLabelValidator.validate(dto.getSizeLabel());
        errors.addAll(stockError);

        return errors;
    }
}
