package pl.mastalerz.threedots.api.validator.review;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.validator.ValidationError;

import java.util.ArrayList;
import java.util.List;

@Component
class ReviewContentValidator {
    private enum Error implements ValidationError {
        CONTENT_BLANK("Kontent nie może być pusty");;

        private final String message;

        Error(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        @Override
        public String getCode() {
            return this.name();
        }
    }

    public List<ValidationError> validate(String value) {
        List<ValidationError> errors = new ArrayList<>();

        if (isNameBlank(value))
            errors.add(Error.CONTENT_BLANK);

        return errors;
    }

    private boolean isNameBlank(String value){
        return StringUtils.isBlank(value);
    }
}
