package pl.mastalerz.threedots.api.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.dto.*;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ProductFacade {
    private final ProductCommandService productCommandService;
    private final ProductQueryService productQueryService;

    public String create(ProductCreateDto dto) {
        return productCommandService.create(dto);
    }

    public void createVariant(Long productId, VariantCreateDto dto){
        productCommandService.createVariant(productId, dto);
    }

    public void deleteVariant(Long productId, Long variantId){
        productCommandService.deleteVariant(productId, variantId);
    }

    public void deleteVariants(Long productId){
        productCommandService.deleteAllVariants(productId);
    }

    public void updateDiscount(Long id, DiscountDto dto) {
        productCommandService.updateDiscount(id, dto);
    }

    public void deleteFromCategory(Long id) {
        productCommandService.deleteFromCategory(id);
    }

    public void deleteModel(Long id){
        productCommandService.deleteModel(id);
    }

    public ProductResponseDto findProduct(Long id){
        return productQueryService.findById(id);
    }

    public ProductSearchResult find(ProductQuery query) {
        return productQueryService.find(query);
    }

    public List<ProductResponseDto> findAllProductsForCategory(Long categoryId){
        return productQueryService.findAllForCategory(categoryId);
    }

    public ProductAndVariantsResponseDto findByIdAllVariants(Long productId){
        return productQueryService.findByIdAllVariants(productId);
    }

    public void addToCategory(Long productId, Long categoryId){
        productCommandService.addToCategory(productId, categoryId);
    }

    public List<ProductResponseDto> findAllModels(){
        return productQueryService.findAllModels();
    }

    public void addReview(Long productId, ReviewCreateDto dto){
        productCommandService.addReview(productId, dto);
    }

    public List<ReviewItemDto> getAllReviews(Long productId) {
        return productCommandService.getAllReviews(productId);
    }

    public void deleteReview(Long productId, Long reviewId){
        productCommandService.deleteReview(productId, reviewId);
    }

    public void deleteAllReviews(Long productId){
        productCommandService.deleteAllReviews(productId);
    }

}
