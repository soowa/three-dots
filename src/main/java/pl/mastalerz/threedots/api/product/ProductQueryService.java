package pl.mastalerz.threedots.api.product;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import pl.mastalerz.threedots.domain.product.ProductRepository;
import pl.mastalerz.threedots.dto.*;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.readmodel.ProductView;
import pl.mastalerz.threedots.readmodel.ProductViewRepository;
import pl.mastalerz.threedots.readmodel.ProductView_;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class ProductQueryService {

    private final ProductRepository repository;
    private final ProductViewRepository viewRepository;

    public ProductSearchResult find(ProductQuery query) {
        final var spec = buildSpec(query);
        final var result = viewRepository.findAll(spec, pageRequest(query));

        return new ProductSearchResult(
                result.stream().map(item -> new ProductSearchItem(
                        item.getProductId(),
                        item.getVariantId(),
                        item.getName(),
                        item.getPrice().toString(),
                        item.getColor(),
                        item.getImageUrl(),
                        item.getDiscount(),
                        item.getDiscountExpirationDate(),
                        item.getCategoryId(),
                        item.getCategory()
                )).toList(),
                result.getNumber(),
                result.getTotalPages()
        );
    }

    private Specification<ProductView> buildSpec(ProductQuery query) {
        return (root, q, cb) -> {
            final var predicates = new ArrayList<>();

            if (query.getMinPrice() != null) {
                predicates.add(
                        cb.ge(root.get(ProductView_.PRICE), query.getMinPrice())
                );
            }

            if (query.getMaxPrice() != null) {
                predicates.add(
                        cb.le(root.get(ProductView_.PRICE), query.getMaxPrice())
                );
            }

            if (query.getColor() != null) {
                predicates.add(
                        cb.equal(root.get(ProductView_.COLOR), query.getColor())
                );
            }

            if (query.getCategoryId() != null) {
                predicates.add(
                        cb.equal(root.get(ProductView_.CATEGORY_ID), query.getCategoryId())
                );
            }

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    private PageRequest pageRequest(ProductQuery query) {
        return PageRequest.of(
                query.getPageNumber(),
                query.getPageSize(),
                Sort.by(query.getOrderDirection(), query.getOrderBy())
        );
    }

    public ProductResponseDto findById(Long id) {
        return repository.findById(id)
                .map(ProductMapper::toProductResponseDto)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));
    }

    public ProductAndVariantsResponseDto findByIdAllVariants(Long id) {
        return repository.findById(id)
                .map(ProductMapper::toProductAndVariantsResponseDto)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));
    }

    public List<ProductResponseDto> findAllForCategory(Long categoryId) {
        return repository.findAllByCategoryId(categoryId)
                .stream()
                .map(ProductMapper::toProductResponseDto)
                .collect(Collectors.toList());
    }

    public List<ProductResponseDto> findAllModels() {
        return repository.findAll()
                .stream()
                .map(ProductMapper::toProductResponseDto)
                .collect(Collectors.toList());
    }

}
