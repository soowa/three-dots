package pl.mastalerz.threedots.api.product;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.api.validator.product.DiscountUpdateValidator;
import pl.mastalerz.threedots.api.validator.product.ProductCreateValidator;
import pl.mastalerz.threedots.api.validator.review.ReviewCreateValidator;
import pl.mastalerz.threedots.api.validator.variant.VariantCreateValidator;
import pl.mastalerz.threedots.domain.category.CategoryRepository;
import pl.mastalerz.threedots.domain.product.*;
import pl.mastalerz.threedots.domain.productReview.ProductReview;
import pl.mastalerz.threedots.domain.productReview.ReviewContent;
import pl.mastalerz.threedots.domain.productReview.ReviewRating;
import pl.mastalerz.threedots.domain.productReview.ReviewRepository;
import pl.mastalerz.threedots.domain.stock.StockRepository;
import pl.mastalerz.threedots.domain.variant.Color;
import pl.mastalerz.threedots.domain.variant.Variant;
import pl.mastalerz.threedots.domain.variant.VariantRepository;
import pl.mastalerz.threedots.dto.*;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.exception.ValidationException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Transactional
class ProductCommandService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final VariantRepository variantRepository;
    private final StockRepository stockRepository;
    private final ReviewRepository reviewRepository;
    private final DiscountUpdateValidator discountUpdateValidator;
    private final ProductCreateValidator productCreateValidator;
    private final VariantCreateValidator createValidator;
    private final ReviewCreateValidator reviewCreateValidator;

    public String create(ProductCreateDto dto) {
        final var product = dto.getCategoryId() != null ? createProductWithCategory(dto) : createProductWithoutCategory(dto);

        var savedProduct = productRepository.save(product);
        return savedProduct.getModelNumber().toString();
    }

    public void createVariant(Long productId, VariantCreateDto dto) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new IllegalArgumentException("Nie ma takiego produktu"));

        throwValidationExceptionIfErrorsExists(createValidator.validate(dto));

        var variant = this.createVariant(product, dto);
        product.addVariant(variant);
    }

    public void deleteVariant(Long productId, Long variantId) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));

        var variant = variantRepository.findById(variantId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego wariantu produktu"));

        stockRepository.deleteAllByVariant(variant);
        product.removeVariant(variant);
    }

    public void deleteAllVariants(Long productId) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));


        product.getVariants()
                .forEach(stockRepository::deleteAllByVariant);

        product.deleteAllVariants();
    }

    public void deleteModel(Long productId) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));

        product.getVariants()
                .forEach(stockRepository::deleteAllByVariant);
        productRepository.deleteById(productId);


    }

    public void updateDiscount(Long productId, DiscountDto discountDto) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));
        throwValidationExceptionIfErrorsExists(discountUpdateValidator.validate(discountDto));

        Discount discount = Discount.of(discountDto.getDiscount());
        LocalDateTime expirationDate = discountDto.getDiscountExpirationDate();

        product.updateDiscount(discount, expirationDate);
    }

    public void addToCategory(Long productId, Long categoryId) {
        var category = categoryRepository
                .findById(categoryId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiej kategorii"));

        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));

        product.setCategory(category);
    }

    public void deleteFromCategory(Long id) {
        var product = productRepository.findById(id)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produkt"));

        product.setCategory(null);
    }

    public void addReview(Long productId, ReviewCreateDto dto) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));

        throwValidationExceptionIfErrorsExists(reviewCreateValidator.validate(dto));
        final var review = this.createReview(dto);

        product.addReview(review);
    }

    public List<ReviewItemDto> getAllReviews(Long productId) {
        final var product = productRepository.findByProductId(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));

        return product.getProductReviews().stream()
                .map(ReviewItemMapper::map)
                .collect(Collectors.toList());
    }

    public void deleteReview(Long productId, Long reviewId) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));

        var review = reviewRepository.findById(reviewId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiej opinii"));

        product.deleteReview(review);
    }

    public void deleteAllReviews(Long productId) {
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu"));


        product.deleteAllReviews();
    }

    private ProductReview createReview(ReviewCreateDto dto) {
        return ProductReview.builder()
                .content(ReviewContent.of(dto.getContent()))
                .rating(ReviewRating.of(dto.getRating()))
                .build();
    }

    private Variant createVariant(Product product, VariantCreateDto dto) {

        Color color = Color.of(dto.getColor());
        UUID variantNumber = UUID.randomUUID();
        String url = null;

        if (StringUtils.isNotBlank(dto.getImageUrl()) ) {
            url = dto.getImageUrl();
        }

        return Variant.builder()
                .color(color)
                .variantNumber(variantNumber)
                .product(product)
                .imageUrl(url)
                .build();
    }

    private Product createProductWithCategory(ProductCreateDto dto) {
        var category = categoryRepository
                .findById(dto.getCategoryId())
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiej kategorii"));

        var productErrors = productCreateValidator.validate(dto);
        throwValidationExceptionIfErrorsExists(productErrors);

        UUID variantNumber = UUID.randomUUID();
        Product product = createProduct(dto);
        product.setModelNumber(variantNumber);
        product.setCategory(category);
        return product;

    }

    private Product createProductWithoutCategory(ProductCreateDto dto) {
        var productErrors = productCreateValidator.validate(dto);
        throwValidationExceptionIfErrorsExists(productErrors);

        UUID variantNumber = UUID.randomUUID();
        Product product = createProduct(dto);
        product.setModelNumber(variantNumber);
        return product;
    }

    private void throwValidationExceptionIfErrorsExists(List<ValidationError> errors) {
        if (!errors.isEmpty()) throw new ValidationException(errors);
    }

    private Product createProduct(ProductCreateDto dto) {
        Name name = Name.of(dto.getName());
        Description description = null;
        Price price = Price.of(dto.getPrice());
        LocalDateTime expiration = null;
        Discount discount = null;

        if (dto.getDiscount() != null){
            discount = Discount.of(dto.getDiscount());
        }

        if (dto.getDiscountExpirationDate() != null){
            expiration = dto.getDiscountExpirationDate();
        }

        if (dto.getDescription() != null) {
            description = Description.of(dto.getDescription());
        }



        return Product.builder()
                .name(name)
                .price(price)
                .description(description)
                .discount(discount)
                .discountExpirationDate(expiration)
                .build();
    }
}
