package pl.mastalerz.threedots.api.cart;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mastalerz.threedots.domain.product.CartRepository;
import pl.mastalerz.threedots.domain.user.Nick;
import pl.mastalerz.threedots.dto.CartMapper;
import pl.mastalerz.threedots.dto.CartResponse;
import pl.mastalerz.threedots.exception.DoesNotExistException;

@Service
@RequiredArgsConstructor
@Transactional
class CartQueryService {

    private final CartRepository cartRepository;

    public CartResponse refreshAndGet(Nick nick) {
        final var cart =  cartRepository.findCartByUser_Nick(nick)
                .orElseThrow(() -> new DoesNotExistException("Nie znaleziono takiego koszyka"));

        cart.refresh();

        return CartMapper.toCartResponse(cart);
    }
}
