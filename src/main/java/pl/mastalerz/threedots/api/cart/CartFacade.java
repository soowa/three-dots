package pl.mastalerz.threedots.api.cart;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.api.TransformCartService;
import pl.mastalerz.threedots.domain.user.Nick;
import pl.mastalerz.threedots.dto.CartAddDto;
import pl.mastalerz.threedots.dto.CartChangeItemAmountDto;
import pl.mastalerz.threedots.dto.CartResponse;
import pl.mastalerz.threedots.security.AuthUserDetails;

@Component
@RequiredArgsConstructor
public class CartFacade {

    private final CartCommandService cartCommandService;
    private final CartQueryService cartQueryService;
    private final TransformCartService transformCartService;

    public CartResponse getCart(Nick nick) {
        return cartQueryService.refreshAndGet(nick);
    }

    public void addProduct(AuthUserDetails user, CartAddDto cartAddDto) {
        cartCommandService.addProduct(user, cartAddDto);
    }

    public String removeProduct(AuthUserDetails user, Long variantId) {
        return cartCommandService.removeProduct(user, variantId);
    }

    public void updateItemAmount(AuthUserDetails user, CartChangeItemAmountDto itemAmountDto) {
        cartCommandService.updateItemAmount(user, itemAmountDto);
    }

    public void decreaseByOne(AuthUserDetails user, Long variantId) {
        cartCommandService.decreaseByOne(user, variantId);
    }

    public void increaseByOne(AuthUserDetails user, Long variantId) {
        cartCommandService.increaseByOne(user, variantId);
    }

    public void transformToOrder(AuthUserDetails user) {
        transformCartService.transform(user);
    }

}
