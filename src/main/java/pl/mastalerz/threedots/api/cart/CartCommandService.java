package pl.mastalerz.threedots.api.cart;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mastalerz.threedots.api.ApiError;
import pl.mastalerz.threedots.domain.product.*;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.size.SizeLabel;
import pl.mastalerz.threedots.domain.size.SizeRepository;
import pl.mastalerz.threedots.domain.stock.Stock;
import pl.mastalerz.threedots.domain.stock.StockRepository;
import pl.mastalerz.threedots.domain.user.Nick;
import pl.mastalerz.threedots.domain.user.UserRepository;
import pl.mastalerz.threedots.dto.CartAddDto;
import pl.mastalerz.threedots.dto.CartChangeItemAmountDto;
import pl.mastalerz.threedots.dto.CartMapper;
import pl.mastalerz.threedots.dto.CartResponse;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.exception.ValidationException;
import pl.mastalerz.threedots.security.AuthUserDetails;

@Service
@RequiredArgsConstructor
@Transactional
class CartCommandService {
    private final CartRepository cartRepository;
    private final ProductRepository productRepository;
    private final StockRepository stockRepository;
    private final SizeRepository sizeRepository;
    private final UserRepository userRepository;

    public String removeProduct(AuthUserDetails user, Long variantId) {
        final var cart = loadCart(user);
        final var cartItem = loadCartItem(cart, variantId);
        final var price = cart.removeItem(cartItem);
        return price.toString();
    }

    public Price addProduct(AuthUserDetails user, CartAddDto cartAddDto) {
        final var size = loadSize(cartAddDto.getSizeRaw());
        final var product = loadProduct(cartAddDto.getProductId());
        final var stock = loadStock(cartAddDto.getVariantId(), size);
        if (stock.isEmpty()) {
            throw new ValidationException(ApiError.STOCK_IS_EMPTY);
        }
        final var cart = loadCart(user);

        var cartItem = cart.getCartItem(cartAddDto.getVariantId())
                .orElseGet(() -> product.cartItemOf(cartAddDto.getVariantId(), size));
        cart.addItem(cartItem);


        if (!stock.hasEnough(cartItem.getAmount())) {
            throw new ValidationException(ApiError.NOT_ENOUGH_IN_STOCK);
        }

        return cartRepository.save(cart).getTotalPrice();
    }

    public void updateItemAmount(AuthUserDetails user, CartChangeItemAmountDto itemAmountDto) {
        final var cart = loadCart(user);
        final var cartItem = loadCartItem(cart, itemAmountDto.getVariantId());
        final var stock = loadStock(cartItem);

        if (!stock.hasEnough(itemAmountDto.getAmount())) {
            throw new ValidationException(ApiError.NOT_ENOUGH_IN_STOCK);
        }

        if (itemAmountDto.getAmount() < 1) {
            throw new ValidationException(ApiError.AMOUNT_CANNOT_BE_LESS_THAN_ONE);
        }

        cartItem.setAmount(itemAmountDto.getAmount());
    }

    public void increaseByOne(AuthUserDetails user, Long variantId) {
        final var cart = loadCart(user);
        final var cartItem = loadCartItem(cart, variantId);
        final var stock = loadStock(cartItem);
        if (!stock.hasEnough(cartItem.getAmount() + 1)) {
            throw new ValidationException(ApiError.NOT_ENOUGH_IN_STOCK);
        }

        cartItem.increase();
    }

    public void decreaseByOne(AuthUserDetails user, Long variantId) {
        final var cart = loadCart(user);
        final var cartItem = loadCartItem(cart, variantId);
        if (cartItem.getAmount() <= 1) {
            throw new ValidationException(ApiError.CART_ITEM_AMOUNT_POSITIVE);
        }

        cartItem.decrease();
    }

    private CartItem loadCartItem(Cart cart, Long variantId) {
        return cart.getCartItem(variantId)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu w koszyku"));
    }

    private Cart loadCart(AuthUserDetails user) {
        return cartRepository.findCartByUser_Nick(Nick.of(user.getNickname()))
                .orElseGet(() -> Cart.createEmptyFor(userRepository.findByNick(Nick.of(user.getNickname())).orElseThrow()));
    }

    private Size loadSize(String sizeRaw) {
        return sizeRepository.findBySizeLabel(SizeLabel.of(sizeRaw))
                .orElseThrow(() -> new DoesNotExistException("No size found for label " + sizeRaw));
    }

    private Product loadProduct(Long productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new DoesNotExistException("No product for id " + productId));
    }

    private Stock loadStock(CartItem cartItem) {
        final var variantNumber = cartItem.getVariant();
        final var size = cartItem.getSize();
        return loadStock(variantNumber.getId(), size);
    }

    private Stock loadStock(Long variantId, Size size) {
        return stockRepository.findByVariantIdAndSize(variantId, size)
                .orElseThrow(() -> new DoesNotExistException("No stock found for variant " + variantId + " for size " + size));
    }
}
