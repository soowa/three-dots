package pl.mastalerz.threedots.api.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mastalerz.threedots.domain.category.CategoryRepository;
import pl.mastalerz.threedots.dto.CategoryMapper;
import pl.mastalerz.threedots.dto.CategoryResponseDto;

import java.util.List;

/*
* 1.Znalezienie wszystkich nazwy kategorii
* 2.Znalezienie wszystkich produktow dla kategorii
* 3.Znalezienie wszystkich produktow dla kategorii (sortowanie i paginacja)
* */

@Service
@RequiredArgsConstructor
class CategoryQueryService {
    private final CategoryRepository repository;

    public List<CategoryResponseDto> findAll(){
        return repository.findAll()
                .stream()
                .map(CategoryMapper::toCategoryResponseDto)
                .toList();
    }
}
