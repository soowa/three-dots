package pl.mastalerz.threedots.api.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.dto.CategoryCreateDto;
import pl.mastalerz.threedots.dto.CategoryResponseDto;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CategoryFacade {

    private final CategoryQueryService queryService;
    private final CategoryCommandService commandService;

    public List<CategoryResponseDto> findAllCategories(){
        return queryService.findAll();
    }

    public void deleteCategory(Long id){
        commandService.deleteCategory(id);
    }

    public Long createCategory(CategoryCreateDto dto) {
        return commandService.create(dto);
    }

    public void changeCategoryName(Long id, String name){
        commandService.changeCategoryName(id, name);
    }

}
