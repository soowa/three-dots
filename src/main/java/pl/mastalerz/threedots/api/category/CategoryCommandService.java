package pl.mastalerz.threedots.api.category;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.api.validator.category.CategoryChangeNameValidator;
import pl.mastalerz.threedots.api.validator.category.CategoryCreateValidator;
import pl.mastalerz.threedots.domain.product.ProductRepository;
import pl.mastalerz.threedots.domain.category.Category;
import pl.mastalerz.threedots.domain.category.CategoryName;
import pl.mastalerz.threedots.domain.category.CategoryRepository;
import pl.mastalerz.threedots.dto.CategoryCreateDto;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.exception.ValidationException;

import java.util.List;

@Transactional
@Service
@RequiredArgsConstructor
class CategoryCommandService {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;
    private final CategoryCreateValidator categoryCreateValidator;
    private final CategoryChangeNameValidator changeNameValidator;

    public Long create(CategoryCreateDto request) {

        throwValidationExceptionIfErrorsExists(categoryCreateValidator.validate(request));

        var categoryName = CategoryName.of(request.getName());
        var category = createCategory(categoryName);

        var saved = categoryRepository.save(category);
        return saved.getId();
    }

    public void deleteCategory(Long id) {
        final var category = categoryRepository.findById(id).orElseThrow(() -> new DoesNotExistException("Nie ma takiej kategorii"));

        productRepository.deleteCategoryFromProducts(category);
        categoryRepository.deleteById(id);
    }

    public void changeCategoryName(Long categoryId, String name) {
        var category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new DoesNotExistException("nie ma takiej kategorii"));

        throwValidationExceptionIfErrorsExists(changeNameValidator.validate(name));

        var newCategoryName = CategoryName.of(name);

        category.updateName(newCategoryName);
    }

    private Category createCategory(CategoryName name) {
        return Category.builder()
                .name(name)
                .build();
    }

    private void throwValidationExceptionIfErrorsExists(List<ValidationError> errors) {
        if (!errors.isEmpty())
            throw new ValidationException(errors);
    }
}
