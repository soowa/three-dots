package pl.mastalerz.threedots.api;

import pl.mastalerz.threedots.api.validator.ValidationError;

public enum ApiError implements ValidationError {

    STOCK_IS_EMPTY("Brak produktu na magazynie"),
    AMOUNT_CANNOT_BE_LESS_THAN_ONE("Wartość nie może być ujemna"),
    CART_ITEM_AMOUNT_POSITIVE("Ilość produktu powinna być większa niż 0"),
    NOT_ENOUGH_IN_STOCK("Niewystarczający stan magazynu"),
    EMPTY_CART("Koszyk jest pusty"),
    NO_ACCESS_TO_RESOURCE("");

    private final String message;

    ApiError(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getCode() {
        return this.name();
    }
}
