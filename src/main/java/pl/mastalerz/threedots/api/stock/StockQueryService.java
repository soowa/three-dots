package pl.mastalerz.threedots.api.stock;

import lombok.*;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.domain.stock.Stock;
import pl.mastalerz.threedots.domain.stock.StockRepository;
import pl.mastalerz.threedots.dto.ProductStockDto;
import pl.mastalerz.threedots.dto.StockMapper;
import pl.mastalerz.threedots.dto.StockResponseDto;
import pl.mastalerz.threedots.dto.StockWithSizes;
import pl.mastalerz.threedots.exception.DoesNotExistException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
class StockQueryService {

    private final StockRepository stockRepository;

    public ProductStockDto findByProductId(Long productId) {
        final var stockForAllProductVariants = stockRepository.findAllByProductId(productId);

        var stocksPerVariant = stockForAllProductVariants.stream()
                .collect(Collectors.groupingBy(stock -> stock.getVariant().getId()));

        return new ProductStockDto(productId,
                stocksPerVariant.entrySet().stream()
                        .map(e -> new StockWithSizes(
                                e.getKey(),
                                colorForVariant(stockForAllProductVariants, e),
                                e.getValue().stream()
                                        .map(StockMapper::toStockResponseDto)
                                        .collect(Collectors.toList())
                        ))
                        .collect(Collectors.toList())
        );

    }

    private String colorForVariant(List<Stock> stockForAllProductVariants, Map.Entry<Long, List<Stock>> e) {
        return stockForAllProductVariants
                .stream()
                .filter(s -> s.getVariant().getId().equals(e.getKey()))
                .findFirst()
                .orElseThrow()
                .getColor()
                .getValue();
    }

    public StockWithSizes findByVariantId(Long variantId) {
        final var all = stockRepository.findAllByVariant_Id(variantId);
        if (all.isEmpty()) {
            return StockWithSizes.empty();
        }

        final var stockPerSize = all.stream()
                .map(StockMapper::toStockResponseDto)
                .collect(Collectors.toList());

        var el = all.get(0);
        return new StockWithSizes(
                el.getVariant().getId(),
                el.getColor().getValue(),
                stockPerSize
        );
    }

    public StockResponseDto findById(Long stockId) {
        return stockRepository.findById(stockId)
                .map(StockMapper::toStockResponseDto)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego wariantu!"));
    }

}
