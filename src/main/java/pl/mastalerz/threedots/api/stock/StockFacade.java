package pl.mastalerz.threedots.api.stock;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.mastalerz.threedots.dto.ProductStockDto;
import pl.mastalerz.threedots.dto.StockCreateDto;
import pl.mastalerz.threedots.dto.StockResponseDto;
import pl.mastalerz.threedots.dto.StockWithSizes;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class StockFacade {
    private final StockCommandService stockCommandService;
    private final StockQueryService stockQueryService;

    public Long create(StockCreateDto dto){
        return stockCommandService.create(dto);
    }

    public void updateAmount(Long stockId, int amount){
        stockCommandService.updateAmount(stockId, amount);
    }

    public void delete(Long id){
        stockCommandService.delete(id);
    }

    public void deleteAll(){
        stockCommandService.deleteAll();
    }

    public void deleteAllForVariant(Long variantId){
        stockCommandService.deleteAllForVariant(variantId);
    }

    public StockWithSizes findByVariantId(Long variantId){
        return stockQueryService.findByVariantId(variantId);
    }

    public StockResponseDto findByStockId(Long stockId){
        return stockQueryService.findById(stockId);
    }

    public ProductStockDto findByProductId(Long productId) {
        return stockQueryService.findByProductId(productId);
    }
}
