package pl.mastalerz.threedots.api.stock;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.mastalerz.threedots.api.validator.ValidationError;
import pl.mastalerz.threedots.api.validator.stock.StockCreateValidator;
import pl.mastalerz.threedots.api.validator.stock.StockUpdateValidator;
import pl.mastalerz.threedots.domain.product.CartItemRepository;
import pl.mastalerz.threedots.domain.size.Size;
import pl.mastalerz.threedots.domain.size.SizeRepository;
import pl.mastalerz.threedots.domain.stock.Stock;
import pl.mastalerz.threedots.domain.stock.StockAmount;
import pl.mastalerz.threedots.domain.stock.StockRepository;
import pl.mastalerz.threedots.domain.variant.Variant;
import pl.mastalerz.threedots.domain.variant.VariantRepository;
import pl.mastalerz.threedots.dto.StockCreateDto;
import pl.mastalerz.threedots.exception.DoesNotExistException;
import pl.mastalerz.threedots.exception.ValidationException;

import java.util.List;

@Component
@RequiredArgsConstructor
@Transactional
class StockCommandService {
    private final StockRepository stockRepository;
    private final SizeRepository sizeRepository;
    private final VariantRepository variantRepository;
    private final StockCreateValidator stockCreateValidator;
    private final StockUpdateValidator stockUpdateValidator;
    private final CartItemRepository cartItemRepository;

    public Long create(StockCreateDto dto){
        final var variant = variantRepository.findById(dto.getVariantId())
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego wariantu"));

        final var size = sizeRepository.findById(dto.getSizeId())
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego rozmiaru"));
        throwValidationExceptionIfErrorsExists(stockCreateValidator.validate(dto));

        final var stock = createStock(size, variant, dto.getAmount());
        final var save = stockRepository.save(stock);

        return save.getId();
    }

    public void updateAmount(Long stockId, int amount){
        final var stock = stockRepository.findById(stockId)
                .orElseThrow(() -> new DoesNotExistException("Nie istnieje taka wartość na magazynie"));
        throwValidationExceptionIfErrorsExists(stockUpdateValidator.validate(amount));

        final var stockAmount = StockAmount.of(amount);
        stock.updateStockAmount(stockAmount);
    }

    public void delete(Long id){
        final var stock = stockRepository.findById(id)
                .orElseThrow(() -> new DoesNotExistException("Nie ma takiego produktu na magazynie"));
        final var variant = stock.getVariant();

        cartItemRepository.deleteByVariantId(variant.getId());
        stockRepository.deleteById(id);
    }

    public void deleteAllForVariant(Long id){
        cartItemRepository.deleteByVariantId(id);
        stockRepository.deleteAllByVariantId(id);
    }

    public void deleteAll(){
        cartItemRepository.deleteAll();
        stockRepository.deleteAll();
    }

    private Stock createStock(Size size, Variant variant, int amount){
        final var stockAmount = StockAmount.of(amount);
        return Stock.builder()
                .size(size)
                .variant(variant)
                .amount(stockAmount)
                .build();
    }

    private void throwValidationExceptionIfErrorsExists(List<ValidationError> errors) {
        if (!errors.isEmpty()) throw new ValidationException(errors);
    }
}
