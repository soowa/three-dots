package pl.mastalerz.threedots.security;

import org.springframework.security.core.context.SecurityContextHolder;

public class UserContextHelper {

    private UserContextHelper() {
    }

    public static AuthUserDetails currentUser() {
        var auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null || !auth.isAuthenticated()) {
            return null;
        }
        return (AuthUserDetails) auth.getPrincipal();
    }
}
