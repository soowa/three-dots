package pl.mastalerz.threedots.security;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.mastalerz.threedots.domain.user.User;
import pl.mastalerz.threedots.domain.user.UserRole;

import java.util.Collection;
import java.util.List;

@Getter
@AllArgsConstructor
public class AuthUserDetails implements UserDetails {

    private Long userId;
    private String nickname;
    private String firstName;
    private String lastName;
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    public static AuthUserDetails createFrom(User account) {
        return new AuthUserDetails(
                account.getId(),
                account.getNick().getValue(),
                account.getFirstName().getValue(),
                account.getLastName().getValue(),
                account.getPassword(),
                List.of(new SimpleGrantedAuthority("ROLE_" + account.getRole().name()))
        );
    }

    public UserRole getRole() {
        var role = authorities.iterator().next();
        return UserRole.valueOf(role.getAuthority().split("_")[1]);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return nickname;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
