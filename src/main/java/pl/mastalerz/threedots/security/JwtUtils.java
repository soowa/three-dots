package pl.mastalerz.threedots.security;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

@Slf4j
@Component
public class JwtUtils {

    @Value("${threedots.app.jwt-secret}")
    private String jwtSecret;

    @Value("${threedots.app.jwt-expiration-ms}")
    private int jwtExpirationMs;

    public String generateJwtToken(AuthUserDetails userDetails) {
        var claims = Jwts.claims().setSubject(userDetails.getUsername());
        insertIfExists(claims, userDetails.getFirstName(), "firstName");
        insertIfExists(claims, userDetails.getLastName(), "lastName");
        insertIfExists(claims, ((SimpleGrantedAuthority) userDetails.getAuthorities().toArray()[0]).getAuthority(), "role");

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().getSubject();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }

    private void insertIfExists(Map<String, Object> map, Object value, String key) {
        if (value != null) {
            map.put(key, value);
        }
    }
}
