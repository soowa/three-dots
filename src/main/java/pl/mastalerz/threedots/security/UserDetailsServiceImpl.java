package pl.mastalerz.threedots.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.mastalerz.threedots.domain.user.Email;
import pl.mastalerz.threedots.domain.user.Nick;
import pl.mastalerz.threedots.domain.user.UserRepository;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final var user = userRepository.findByNick(Nick.of(username))
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));

        return AuthUserDetails.createFrom(user);
    }
}
