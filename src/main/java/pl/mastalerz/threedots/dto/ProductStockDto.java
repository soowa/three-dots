package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ProductStockDto {
    private Long productId;
    private List<StockWithSizes> data;
}
