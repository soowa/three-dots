package pl.mastalerz.threedots.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long Id;
    private String email;
    private String username;

    private String firstName;
    private String lastName;

    private String phoneNumber;

    private String zipCode;
    private String city;
    private String street;
    private String apartmentNumber;
    private String buildingNumber;

}
