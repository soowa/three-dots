package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.product.Product;

public class ProductMapper {

    public static ProductResponseDto toProductResponseDto(Product product) {
        return ProductResponseDto.builder()
                .productId(product.getId())
                .categoryId(product.getCategory() != null ? product.getCategory().getId() : null)
                .name(product.getName().getValue())
                .description(product.getDescription() != null ? product.getDescription().getValue() : null)
                .price(product.getPrice().getValue())
                .discount(product.getDiscount() != null ? product.getDiscount().getValue() : null)
                .discountExpirationDate(product.getDiscountExpirationDate())
                .build();
    }

    public static ProductAndVariantsResponseDto toProductAndVariantsResponseDto(Product product) {
        return ProductAndVariantsResponseDto.builder()
                .categoryId(product.getCategory() != null ? product.getCategory().getId() : null)
                .modelName(product.getName().getValue())
                .modelNumber(product.getModelNumber().toString())
                .description(product.getDescription() != null ? product.getDescription().getValue() : null)
                .variants(product.getVariants().stream().map(VariantMapper::toVariantResponseDto).toList())
                .build();
    }

}
