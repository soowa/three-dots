package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.variant.Variant;

public class VariantMapper {

    private VariantMapper() {
    }

    public static VariantResponseDto toVariantResponseDto(Variant variant){
        return VariantResponseDto.builder()
                .variantId(variant.getId())
                .color(variant.getColor().getValue())
                .imageUrl(variant.getImageUrl())
                .build();

    }
}
