package pl.mastalerz.threedots.dto;

import lombok.Getter;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;

@Getter
public class ProductQuery {
    private String color;
    private BigDecimal minPrice;
    private BigDecimal maxPrice;
    private Long categoryId;

    private int pageNumber;
    private int pageSize;

    private String orderBy = "price";
    private Sort.Direction orderDirection = Sort.Direction.DESC;
}
