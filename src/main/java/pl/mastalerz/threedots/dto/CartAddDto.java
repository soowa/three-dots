package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CartAddDto {
    private Long productId;
    private Long variantId;
    private String sizeRaw;
}
