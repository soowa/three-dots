package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.size.Size;

public class SizeMapper {
    public static SizeResponseDto toSizeResponseDto(Size size){
        return SizeResponseDto.builder()
                .id(size.getId())
                .sizeLabel(size.getSizeLabel().getValue())
                .build();
    }

}
