package pl.mastalerz.threedots.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@Builder
@NoArgsConstructor()
@AllArgsConstructor()
@JsonInclude(JsonInclude.Include.NON_NULL)
public class StockWithSizes {
    private Long variantId;
    private String color;
    private List<StockResponseDto> sizes;

    public static StockWithSizes empty() {
        return new StockWithSizes();
    }
}
