package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.category.Category;

public class CategoryMapper {

    public static CategoryResponseDto toCategoryResponseDto(Category category) {
        return CategoryResponseDto.builder()
                .id(category.getId())
                .name(category.getName().getValue())
                .build();
    }


}
