package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.size.Size;

public class SizeAndAmountMapper {

    public static SizeAndAmountDto toSizeAndAmountDto(int amount, Size size){
        return SizeAndAmountDto.builder()
                .amount(amount)
                .size(SizeMapper.toSizeResponseDto(size))
                .build();
    }

}
