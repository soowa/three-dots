package pl.mastalerz.threedots.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductUpdateDto {
    private int stock;
    private BigDecimal price;
    private BigDecimal discount;
    private LocalDateTime discountExpirationDate;
}
