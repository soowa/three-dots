package pl.mastalerz.threedots.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DiscountDto {
    private BigDecimal discount;
    private LocalDateTime discountExpirationDate;
    
}
