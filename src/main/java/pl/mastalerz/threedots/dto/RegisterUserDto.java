package pl.mastalerz.threedots.dto;

import lombok.Data;

@Data
public class RegisterUserDto {

    private String email;
    private String username;
    private String password;

    private String firstName;
    private String lastName;

    private String phoneNumber;

    private String zipCode;
    private String city;
    private String street;
    private String apartmentNumber;
    private String buildingNumber;

}
