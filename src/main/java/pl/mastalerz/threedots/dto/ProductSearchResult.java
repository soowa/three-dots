package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ProductSearchResult {

    private List<ProductSearchItem> data;

    private int pageNumber;
    private int totalPages;
}
