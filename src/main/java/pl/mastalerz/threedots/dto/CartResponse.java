package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class CartResponse {
    private BigDecimal totalPrice;
    private List<CartItemResponse> items;
}
