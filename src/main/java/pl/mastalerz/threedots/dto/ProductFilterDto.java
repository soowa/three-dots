package pl.mastalerz.threedots.dto;

import lombok.*;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductFilterDto {
    private String name;//size
    private String value;//36

}
