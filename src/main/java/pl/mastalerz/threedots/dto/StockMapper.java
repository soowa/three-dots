package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.stock.Stock;

public class StockMapper {
    public static StockResponseDto toStockResponseDto(Stock stock){
        return StockResponseDto.builder()
                .stockId(stock.getId())
                .size(stock.getSize().getSizeLabel().getValue())
                .amount(stock.getAmount().getValue())
                .build();
    }
}
