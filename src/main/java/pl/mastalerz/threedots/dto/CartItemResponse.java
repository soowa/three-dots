package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class CartItemResponse {
    private int amount;
    private Long productId;
    private String name;
    private BigDecimal price;
    private String description;
    private String imageUrl;
    private BigDecimal discount;
    private LocalDateTime discountExpirationDate;
    private Long variantId;
    private String color;
    private String size;;
}
