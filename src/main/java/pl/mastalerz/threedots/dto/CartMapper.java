package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.product.Cart;
import pl.mastalerz.threedots.domain.product.CartItem;

public class CartMapper {
    public static CartItemResponse toCartItemResponse(CartItem cartItem){
        return new CartItemResponse(
                cartItem.getAmount(),
                cartItem.getProduct().getId(),
                cartItem.getProduct().getName().getValue(),
                cartItem.getPrice().getValue(),
                cartItem.getProduct().getDescription() != null ? cartItem.getProduct().getDescription().getValue() : null,
                cartItem.getVariant().getImageUrl(),
                cartItem.getProduct().getDiscount() != null ? cartItem.getProduct().getDiscount().getValue() : null,
                cartItem.getProduct().getDiscountExpirationDate(),
                cartItem.getVariant().getId(),
                cartItem.getVariant().getColor().getValue(),
                cartItem.getSize().getSizeLabel().getValue()
        );
    }

    public static CartResponse toCartResponse(Cart cart){
        return new CartResponse(
                cart.getTotalPrice().getValue(),
                cart.getItems().stream().map(CartMapper::toCartItemResponse).toList()
        );
    }
}
