package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.order.Order;

public class OrderDtoMapper {

    private OrderDtoMapper() {
    }

    public static OrderDto map(Order order) {
        return OrderDto.builder()
                .number(order.getNumber().getValue())
                .city(order.getAddress().getCity().getValue())
                .zipCode(order.getAddress().getZipCode().getValue())
                .street(order.getAddress().getStreet().getValue())
                .buildingNumber(order.getAddress().getBuildingNumber().getValue())
                .apartmentNumber(order.getAddress().getApartmentNumber().getValue())
                .status(order.getStatus())
                .clientFirstname(order.getOwnerFirstName())
                .clientLastname(order.getOwnerLastName())
                .userId(order.getOwnerId())
                .build();
    }
}
