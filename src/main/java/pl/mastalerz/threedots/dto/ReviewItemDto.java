package pl.mastalerz.threedots.dto;

import lombok.*;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ReviewItemDto {
    private Long id;
    private String content;
    private Integer rating;
}
