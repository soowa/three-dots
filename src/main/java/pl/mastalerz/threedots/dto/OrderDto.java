package pl.mastalerz.threedots.dto;

import lombok.*;
import pl.mastalerz.threedots.domain.order.OrderStatus;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class OrderDto {

    private String number;

    private String city;
    private String zipCode;
    private String street;
    private String buildingNumber;
    private String apartmentNumber;
    private OrderStatus status;

    private String clientFirstname;
    private String clientLastname;
    private Long userId;
}
