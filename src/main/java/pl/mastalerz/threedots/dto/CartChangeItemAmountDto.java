package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CartChangeItemAmountDto {
    private Long variantId;
    private int amount;
}
