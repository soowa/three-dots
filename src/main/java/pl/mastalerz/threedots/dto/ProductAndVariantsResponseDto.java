package pl.mastalerz.threedots.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductAndVariantsResponseDto {
    private Long categoryId;
    private String description;
    private String modelNumber;
    private String modelName;
    private List<VariantResponseDto> variants;

}
