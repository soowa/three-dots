package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegisterUserResult {

    private Long userId;
}
