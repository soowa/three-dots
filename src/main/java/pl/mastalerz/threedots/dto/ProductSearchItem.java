package pl.mastalerz.threedots.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ProductSearchItem {
    private Long productId;
    private Long variantId;
    private String name;
    private String price;
    private String color;
    private String imageUrl;
    private BigDecimal discount;
    private LocalDateTime discountExpirationDate;
    private Long categoryId;
    private String category;
}
