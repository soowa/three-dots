package pl.mastalerz.threedots.dto;

import lombok.*;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ReviewCreateDto {
    private String content;
    private Integer rating;

}
