package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.productReview.ProductReview;

public class ReviewItemMapper {

    private ReviewItemMapper() {
    }

    public static ReviewItemDto map(ProductReview review) {
        return ReviewItemDto.builder()
                .id(review.getId())
                .content(review.getContent().getValue())
                .rating(review.getRating().getValue())
                .build();
    }
}
