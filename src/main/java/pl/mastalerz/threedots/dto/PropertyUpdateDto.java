package pl.mastalerz.threedots.dto;

import lombok.*;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyUpdateDto {
    private String name;
    private String value;
}
