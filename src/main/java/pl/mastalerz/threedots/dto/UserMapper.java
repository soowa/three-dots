package pl.mastalerz.threedots.dto;

import pl.mastalerz.threedots.domain.user.User;

public class UserMapper {

    public static UserDto toUserDto(User user){
        return UserDto.builder()
                .Id(user.getId())
                .firstName(user.getFirstName().getValue())
                .lastName(user.getLastName().getValue())
                .username(user.getNick().getValue())
                .email(user.getEmail().getValue())
                .phoneNumber(user.getPhoneNumber().getValue())
                .city(user.getAddress().getCity().getValue())
                .street(user.getAddress().getStreet().getValue())
                .city(user.getAddress().getCity().getValue())
                .zipCode(user.getAddress().getZipCode().getValue())
                .apartmentNumber(user.getAddress().getApartmentNumber().getValue())
                .buildingNumber(user.getAddress().getBuildingNumber().getValue())
                .build();
    }

}
