package pl.mastalerz.threedots.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductCreateDto {
    private Long categoryId;
    private String name;
    private BigDecimal price;
    private String description;
    private BigDecimal discount;
    private String imgUrl;
    private LocalDateTime discountExpirationDate;
}
