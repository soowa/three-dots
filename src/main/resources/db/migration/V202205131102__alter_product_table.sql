ALTER TABLE "product"
    DROP COLUMN size,
    DROP COLUMN color,
    DROP COLUMN variant_number,
    DROP CONSTRAINT fk_product_stock;
