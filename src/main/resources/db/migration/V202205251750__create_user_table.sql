CREATE TABLE "user" (
                           id bigserial PRIMARY KEY,
                           nick VARCHAR ( 50 ) NOT NULL,
                           email VARCHAR ( 50 ) NOT NULL,
                           first_name VARCHAR ( 50 ) NOT NULL,
                           last_name VARCHAR ( 50 ) NOT NULL,
                           phone_number VARCHAR ( 10 ),
                           created_at timestamp,
                           updated_at timestamp
);