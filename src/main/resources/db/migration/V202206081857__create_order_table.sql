CREATE TABLE "order"
(
    id bigserial PRIMARY KEY,
    owner_id bigint,
    number VARCHAR ( 50 ),
    status VARCHAR ( 50 ),
    first_name VARCHAR ( 50 ) NOT NULL,
    last_name VARCHAR ( 50 ) NOT NULL,
    city VARCHAR ( 50 ) NOT NULL,
    zip_code VARCHAR ( 8 ) NOT NULL,
    post_office VARCHAR ( 50 ) NOT NULL,
    street VARCHAR ( 50 ) NOT NULL,
    building_number VARCHAR ( 10 ) NOT NULL,
    apartment_number VARCHAR ( 10 ),
    created_at timestamp,
    updated_at timestamp
)