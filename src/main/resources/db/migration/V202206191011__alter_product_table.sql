DROP VIEW IF EXISTS product_view;

ALTER TABLE product
    DROP COLUMN image_url;

ALTER TABLE variant
    ADD COLUMN image_url varchar;