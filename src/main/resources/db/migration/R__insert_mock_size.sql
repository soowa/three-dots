DELETE FROM "size";

INSERT INTO "size"(id, size_label, updated_at, created_at)
VALUES (1, '36', current_timestamp, current_timestamp),
       (2, '38', current_timestamp, current_timestamp),
       (3, '40', current_timestamp, current_timestamp),
       (4, '42', current_timestamp, current_timestamp),
       (5, '44', current_timestamp, current_timestamp),
       (6, '46', current_timestamp, current_timestamp),
       (7,'48', current_timestamp, current_timestamp)
;