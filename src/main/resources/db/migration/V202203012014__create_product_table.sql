CREATE TABLE "product" (
    id bigserial PRIMARY KEY,
    name VARCHAR ( 50 ) NOT NULL,
    price MONEY NOT NULL,
    created_at DATE NOT NULL,
    updated_at DATE,
    published_at DATE,
    description TEXT,
    discount MONEY,
    discount_expiration_date DATE
);