CREATE TABLE "collection_product" (
    product_id BIGINT,
    collection_id BIGINT,
    CONSTRAINT fk_product FOREIGN KEY (product_id) REFERENCES "product"(id),
    CONSTRAINT fk_collection FOREIGN KEY (collection_id) REFERENCES "collection"(id)
)