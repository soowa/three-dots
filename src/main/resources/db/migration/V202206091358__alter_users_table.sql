ALTER TABLE users
    ADD COLUMN cart_id BIGINT,
    ADD CONSTRAINT fk_cart
        FOREIGN KEY (cart_id)
            REFERENCES "cart" (id);