CREATE TABLE "address" (
                           id bigserial PRIMARY KEY,
                           city VARCHAR ( 50 ),
                           zip_code VARCHAR ( 8 ),
                           post_office VARCHAR ( 50 ),
                           street VARCHAR ( 50 ),
                           building_number VARCHAR ( 10 ),
                           apartment_number VARCHAR ( 10 ),
                           created_at timestamp,
                           updated_at timestamp
);

ALTER TABLE "user"
    ADD COLUMN address_id BIGINT,
    ADD CONSTRAINT address_id FOREIGN KEY (address_id) REFERENCES "user"(id);