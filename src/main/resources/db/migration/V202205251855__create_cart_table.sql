CREATE TABLE "cart" (
                           id bigserial PRIMARY KEY,
                           total_price DECIMAL,
                           created_at timestamp,
                           updated_at timestamp
);

ALTER TABLE "cart_item"
    ADD COLUMN cart_id BIGINT,
    ADD CONSTRAINT fk_cart
        FOREIGN KEY (cart_id)
            REFERENCES "cart" (id)
;