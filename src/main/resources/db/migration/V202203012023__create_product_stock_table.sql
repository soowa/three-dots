CREATE TABLE "product_stock" (
  id BIGSERIAL PRIMARY KEY,
  stock SMALLINT
);
