ALTER TABLE "product"
    ADD COLUMN stock_id BIGINT;

ALTER TABLE "product"
    ADD CONSTRAINT fk_product_stock FOREIGN KEY (stock_id) REFERENCES "product_stock"(id);

