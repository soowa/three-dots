ALTER TABLE cart
    DROP CONSTRAINT fk_user,
    DROP COLUMN user_id;