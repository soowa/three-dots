UPDATE users SET address_id = null;

DELETE FROM users;

ALTER TABLE users
    DROP CONSTRAINT address_id;

ALTER TABLE users
    ADD CONSTRAINT address_id_fk
        FOREIGN KEY (address_id)
            REFERENCES address(id);
