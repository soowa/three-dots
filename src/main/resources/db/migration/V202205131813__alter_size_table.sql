ALTER TABLE "size"
    ADD COLUMN created_at timestamp,
    ADD COLUMN updated_at timestamp;
;
