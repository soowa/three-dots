CREATE TABLE "cart_item" (
                           id bigserial PRIMARY KEY,
                           variant_id BIGINT,
                           product_id BIGINT,
                           size_id BIGINT,
                           amount     SMALLINT,
                           created_at timestamp,
                           updated_at timestamp,
                           CONSTRAINT fk_variant
                               FOREIGN KEY (variant_id)
                                   REFERENCES "variant" (id),
                           CONSTRAINT fk_product
                               FOREIGN KEY (product_id)
                                   REFERENCES "product" (id),
                           CONSTRAINT fk_size
                               FOREIGN KEY (size_id)
                                   REFERENCES "size" (id)
);