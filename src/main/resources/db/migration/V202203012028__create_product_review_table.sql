CREATE TABLE "product_review" (
    id      BIGSERIAL PRIMARY KEY,
    content TEXT,
    rating  SMALLINT
);
