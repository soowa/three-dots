ALTER TABLE "cart"
    ADD COLUMN user_id BIGINT,
    ADD CONSTRAINT fk_user
        FOREIGN KEY (user_id)
            REFERENCES "user" (id)
;