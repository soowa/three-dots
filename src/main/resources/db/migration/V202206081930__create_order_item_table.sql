CREATE TABLE "order_item"
(
    id bigserial PRIMARY KEY,
    order_id bigint,
    name VARCHAR ( 50 ) NOT NULL,
    amount smallint,
    variant_number UUID NOT NULL,
    size VARCHAR ( 5 ),
    color VARCHAR ( 50 ),
    unit_price numeric,
    total_price numeric,
    created_at timestamp,
    updated_at timestamp,
    CONSTRAINT fk_order
        FOREIGN KEY (order_id)
            REFERENCES "order" (id)
)

