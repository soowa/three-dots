ALTER TABLE "product_stock"
    ADD COLUMN variant_id BIGINT,
    ADD COLUMN size_id    BIGINT,
    ADD CONSTRAINT fk_variant
        FOREIGN KEY (variant_id)
            REFERENCES "variant" (id),
    ADD CONSTRAINT fk_size
        FOREIGN KEY (size_id)
            REFERENCES "size" (id)
;
