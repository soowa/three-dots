DROP VIEW IF EXISTS product_view;



CREATE OR REPLACE VIEW product_view AS
SELECT
    v.id as variant_id,
    p.id as product_id,
    p.name as name,
    v.color,
    p.price,
    p.description as description,
    p.discount as discount,
    v.image_url,
    p.discount_expiration_date as discount_expiration_date,
    p.category_id as category_id,
    c.name as category_name
FROM product p
         JOIN variant v on p.id = v.product_id
         LEFT JOIN category c on c.id = p.category_id;

