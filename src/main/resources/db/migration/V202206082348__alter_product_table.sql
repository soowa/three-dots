DROP VIEW If exists "product_view";

ALTER TABLE "product"
    ALTER COLUMN discount type numeric;
