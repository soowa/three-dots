CREATE TABLE "variant"
(
    id             BIGSERIAL PRIMARY KEY,
    variant_number UUID,
    color          VARCHAR(50),
    product_id     BIGINT,
    CONSTRAINT fk_product
        FOREIGN KEY (product_id)
            REFERENCES "product" (id)
)