CREATE TABLE "collection" (
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR (50) NOT NULL,
    description TEXT,
    created_at DATE,
    expiration_date DATE
)