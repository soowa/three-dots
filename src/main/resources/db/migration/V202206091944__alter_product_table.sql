DROP VIEW IF EXISTS product_view;

ALTER TABLE product
    ALTER COLUMN image_url TYPE varchar;