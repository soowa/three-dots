DELETE FROM "category";

INSERT INTO "category"(id, name, created_at, updated_at)
VALUES (1, 'Botki', current_timestamp, current_timestamp),
       (2, 'Sportowe', current_timestamp, current_timestamp),
       (3, 'Trampki', current_timestamp, current_timestamp),
       (4, 'Sneakersy', current_timestamp, current_timestamp),
       (5, 'Klapki', current_timestamp, current_timestamp),
       (6, 'Kapcie', current_timestamp, current_timestamp),
       (7, 'Kalosze', current_timestamp, current_timestamp);