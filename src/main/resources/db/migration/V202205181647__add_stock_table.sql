CREATE TABLE "stock"
(
    id         BIGSERIAL PRIMARY KEY,
    amount     SMALLINT,
    size_id    BIGINT,
    variant_id BIGINT,
    created_at timestamp,
    updated_at timestamp,
    CONSTRAINT fk_variant
        FOREIGN KEY (variant_id)
            REFERENCES "variant" (id),
    CONSTRAINT fk_size
        FOREIGN KEY (size_id)
            REFERENCES "size" (id)
)