CREATE TABLE "product_property" (
    id    BIGSERIAL PRIMARY KEY,
    name  VARCHAR(50) NOT NULL,
    value varchar(50) NOT NULL
)