with admin_address as (
    INSERT INTO "address" (city, zip_code, street, building_number, apartment_number, created_at, updated_at)
        values ('krakow', '11-111', 'ulicawska', '11', '22', current_timestamp, current_timestamp)
        returning id as rosomak
)
INSERT INTO users(id, nick, email, first_name, last_name, phone_number, created_at, updated_at, address_id, role, password)
SELECT 100,
       'admin',
       'e@e.pl',
       'admin',
       'adminowski',
       '1234567899',
       current_timestamp,
       current_timestamp,
       rosomak,
       'ADMIN',
       '$2a$10$V0NjaFuaKL/BUICXqukiS.Txp3HBfdRtm1OZgkDDDVcApzNvoekOO'
FROM admin_address;

-- Ustawienie sekwencji jako ostatnia wartość, aby dla nowych użytkowników zaczynał od 101 itd.
SELECT setval('user_id_seq', 100);
