ALTER TABLE "product"
    ALTER COLUMN created_at TYPE timestamp,
    ALTER COLUMN updated_at TYPE timestamp,
    ALTER COLUMN published_at TYPE timestamp,
    ALTER COLUMN discount_expiration_date TYPE timestamp;

ALTER TABLE "product_property"
    ADD COLUMN created_at timestamp,
    ADD COLUMN updated_at timestamp;

ALTER TABLE "product_stock"
    ADD COLUMN created_at timestamp,
    ADD COLUMN updated_at timestamp;

ALTER TABLE "product_review"
    ADD COLUMN created_at timestamp,
    ADD COLUMN updated_at timestamp;

ALTER TABLE "category"
    ADD COLUMN created_at timestamp,
    ADD COLUMN updated_at timestamp;