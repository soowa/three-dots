#
# Build stage
#
FROM maven:3.8.1-openjdk-17 AS build
WORKDIR /workspace/app

COPY src src
COPY pom.xml .
RUN mvn clean package
RUN mkdir -p build/libs/dependency && (cd build/libs/dependency; jar -xf /workspace/app/target/*exec.jar)

#
# Package stage
#
FROM openjdk:17

ENV DEPENDENCY=/workspace/app/build/libs/dependency

COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app

RUN groupadd --system spring && adduser --system spring -g spring
USER spring:spring


ENTRYPOINT ["java", "-cp", "app:app/lib/*", "pl.mastalerz.threedots.ThreeDotsApplication"]